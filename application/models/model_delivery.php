<?php
/**
 * Created by PhpStorm.
 * User: artem
 * Date: 8/14/2018
 * Time: 6:18 PM
 */

use NovaPoshta\Config;

class model_delivery extends Model
{

    public function __construct()
    {
        Config::setApiKey(ANP);
        Config::setFormat(Config::FORMAT_JSONRPC2);
        Config::setLanguage(Config::LANGUAGE_UA);
    }


    public function updateGroup($arr, $bool = null)
    {
        try {
            $connect = new Database(HOST, DB, USER, PASS);
            $query = $connect->db->prepare('SELECT * FROM dispatch WHERE inv_num_china="' . $arr['inv_num_china'] . '"');
            $query->execute();
            $data = $query->fetchAll();
            $data = [
                'recipient_firstname' => $arr['firstname'],
                'recipient_lastname' => $arr['surname'],
                'city' => $arr['city'],
                'id' => $data[0]['id_group'],
                'form_save' => true
            ];
            if (!isset($bool)) {
                $data['secession'] = $arr['secession'];
            } else {
                $street = explode(".",$arr['address']);
                $street =$street[0] .",". $street[1] .",". $arr['house'] . "," . $arr['number_appart'];
                $data['secession'] = $street;
            }
            $sql = "UPDATE group_dispatch SET 
                        secession = :secession,
                        recipient_firstname = :recipient_firstname,
                        recipient_lastname = :recipient_lastname,
                        city = :city,
                        form_save = :form_save
                    WHERE id = :id";
            $statement = $connect->db->prepare($sql);
            $statement->execute($data);
            return TRUE;
        } catch (Exception $e) {
            echo $e->getMessage();
            return FALSE;
        }
    }

    public function genOptionRtc($data, $seces)
    {
        $str = '';
        $connect = new Database(HOST, DB, USER, PASS);
        $query = $connect->db->prepare("SELECT * FROM secession WHERE id_city=" . $data['id'] . " and description LIKE \"%" . $seces . "%\"");
        $query->execute();
        $data = $query->fetchAll();
        for ($i = 0; $i < 10; $i++) {
            $str .= "<option value=\"" . $data[$i]['description'] . "\">";
        }
        return $str;
    }

    public function genOptionSec($data)
    {
        $str = '';
        $connect = new Database(HOST, DB, USER, PASS);
        $query = $connect->db->prepare("SELECT * FROM secession WHERE id_city=" . $data['id']);
        $query->execute();
        $data = $query->fetchAll();
        for ($i = 0; $i < 10; $i++) {
            $str .= "<option value=\"" . $data[$i]['description'] . "\">";
        }
        return $str;
    }

    public function genOptionCity($data)
    {
        $str = '';
        for ($i = 0; $i < 10; $i++) {
            $str .= "<option value=\"" . $data[$i]['description'] . "\">";
        }
        return $str;
    }

    public function getAddress($data,$address){
        $arr = [];
        $str = "
        {
            \"apiKey\": \"df728330f70c4af25d66d9f8b1a5a191\",
            \"modelName\": \"Address\",
            \"calledMethod\": \"getStreet\",
            \"methodProperties\": {
                \"CityRef\":\"{$data['ref']}\"
            }
        }
        ";
        $curl = curl_init('http://testapi.novaposhta.ua/v2.0/json/Address/getStreet/');
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_HTTPHEADER, [
            'Content-Type: application/json'
        ]);
        curl_setopt($curl, CURLOPT_POST, 1);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $str);
        $result = curl_exec($curl);
        $result = json_decode($result, true);
        curl_close($curl);
        $data = $result['data'];
        foreach ($data as $key => $value){
            if(!preg_match("/{$address}/i",$value['Description'])){
                unset($result['data'][$key]);
            }else{
                $arr[] = $key;
            }
        }
        $data = $result['data'];
        if(empty($data)){
            return "Улица не найдена";
        }
        $str = '';
        foreach ($arr as $key => $value){
            $str .= "<option value=\"" .$data[$value]['StreetsType']. $data[$value]['Description'] . "\">";
            if($key === 10){
                return $str;
            }
        }
        return $str;
    }

    public function getCity($city)
    {
        $connect = new Database(HOST, DB, USER, PASS);
        $query = $connect->db->prepare("SELECT * FROM city WHERE description LIKE \"" . $city . "%\"");
        $query->execute();
        $data = $query->fetchAll();
        if(!empty($data)){
            return $data;
        }else{
            return false;
        }
    }

    public function getForm($arr){
        $str = "<form name=\"deliveryform\" action=\"/delivery\" method=\"POST\" onsubmit=\"return retSub()\" id=\"form\">
            <h3>Номер накладной: {$arr['num_delivery']}</h3>
            <input type=\"text\" hidden=\"hidden\" name=\"inv_num_china\" value=\"{$arr['num_delivery']}\">
            <label for=\"firstname\">Імя</label>
            <input style=\"margin-top:8px;\" type=\"text\" name=\"firstname\" id=\"firstname\"
                   value=\"{$arr['recipient_firstname']}\" required onkeyup=\"valFirst(this)\" autocomplete=\"off\"
                   readonly>
              <input type=\"text\" hidden=\"hidden\" name=\"width\" id=\"width\" value=\"{$arr['width']}\">    
            <div id=\"firstname-error\" style=\"display:none;margin-bottom: 8px;color: orangered;\">Заборонено цифри та
                латинські символи
            </div>
            <label for=\"surname\">Прізвище</label>
            <input style=\"margin-top:8px;\" type=\"text\" name=\"surname\" id=\"surname\"
                   value=\"{$arr['recipient_lastname']}\" required autocomplete=\"off\" onkeyup=\"valSurname(this)\" readonly>
            <div id=\"surname-error\" style=\"display:none;margin-bottom: 8px;color: orangered;\">Заборонено цифри та
                латинські символи
            </div>
            <p>
                <label for=\"city\">Місто</label>
                <input style=\"margin-top:8px;\" type=\"text\" name=\"city\" list=\"city_list\" id=\"city\"
                       onkeyup=\"getCity(this)\"
                       required autocomplete=\"off\" onblur=\"rowCity(this)\">
            <div id=\"city-error\" style=\"display:none;margin-bottom: 8px;color: orangered;\">Заборонено цифри та латинські
                символи
            </div>
            <div id=\"city-error-datalist\" style=\"display:none;margin-bottom: 8px;color: orangered;\">Не вірно введено
                місто
            </div>
            <div id=\"city-error-not-datalist\" style=\"display:none;margin-bottom: 8px;color: orangered;\">Виберіть місто із списку
            </div>
            <datalist id=\"city_list\">
                <option value=\"Київ\">
                <option value=\"Чернігів\">
                <option value=\"Львів\">
                <option value=\"Харків\">
                <option value=\"Одеса\">
                <option value=\"Дніпро\">
            </datalist>
            </p>
            ";
        if($arr['width'] <= 2){
            $str .= "<p>
                <label><input style=\"margin-right: 5px;\" name=\"type_del\" type=\"radio\" value=\"type_address\"
                              onclick=\"showAddress(this)\" id=\"type_address\">Адреса</label>
                <label><input style=\"margin-left: 20px; margin-right: 5px;\" name=\"type_del\" type=\"radio\"
                              value=\"type_secession\" onclick=\"showSuc(this)\" id=\"type_del\" checked>Відділення Нової Пошти</label>
            </p>";
            $str .= "
            <p>
            <div id=\"secession_div\">
                <label for=\"secession\">Отделение</label>
                <input style=\"margin-top:8px;\" type=\"text\" name=\"secession\" list=\"secession_list\" id=\"secession\"
                       onkeyup=\"getSec(this)\" onfocus=\"showSucInput()\" autocomplete=\"off\">
                <div id=\"secession-error\" style=\"display:none;margin-bottom: 8px;color: orangered;\">Заборонено цифри та латинські
                    символи
                </div>
                <div id=\"secession-error-datalist\" style=\"display:none;margin-bottom: 8px;color: orangered;\">Не вірно введено
                    відділення
                </div>
                <div id=\"secession-error-not-datalist\" style=\"display:none;margin-bottom: 8px;color: orangered;\">Виберіть відділення із списку
                </div>
                <datalist id=\"secession_list\"\">
                </datalist>
            </div>
            <div id=\"address_div\" style=\"display: none;\">
                <label for=\"address\">Адрес</label>
                <input style=\"margin-top:8px;\" type=\"text\" name=\"address\" id=\"address\" list=\"address_list\"
                       onkeyup=\"getAddress()\" autocomplete=\"off\">
                <datalist id=\"address_list\">
                </datalist>
                <label for=\"address\">Номер дома</label>
                <input style=\"margin-top:8px;\" type=\"text\" name=\"house\" id=\"house\" autocomplete=\"off\">
                <label for=\"address\">Квартира</label>
                <input style=\"margin-top:8px;\" type=\"text\" name=\"number_appart\" id=\"address\" autocomplete=\"off\">
            </div>
            </p>";
        }else{
            $str .= "<p>
                <label><input style=\"margin-right: 5px;\" name=\"type_del\" type=\"radio\" value=\"type_address\"
                              onclick=\"showAddress(this)\" id=\"type_address\">Адреса</label>
                <label><input style=\"margin-left: 20px; margin-right: 5px;\" name=\"type_del\" type=\"radio\"
                              value=\"type_secession\" onclick=\"showSuc(this)\" id=\"type_del\">Відділення Нової Пошти</label>
            </p>";
            $str .= "
            <p>
            <div id=\"secession_div\">
                <label for=\"secession\" style=\"display: none;\">Отделение</label>
                <input style=\"margin-top:8px;\" type=\"text\" name=\"secession\" list=\"secession_list\" id=\"secession\"
                       onkeyup=\"getSec(this)\" onfocus=\"showSucInput()\" autocomplete=\"off\">
                <div id=\"secession-error\" style=\"display:none;margin-bottom: 8px;color: orangered;\">Заборонено цифри та латинські
                    символи
                </div>
                <div id=\"secession-error-datalist\" style=\"display:none;margin-bottom: 8px;color: orangered;\">Не вірно введено
                    відділення
                </div>
                <div id=\"secession-error-not-datalist\" style=\"display:none;margin-bottom: 8px;color: orangered;\">Виберіть відділення із списку
                </div>
                <datalist id=\"secession_list\"\">
                </datalist>
            </div>
            <div id=\"address_div\" style=\"display: none;\">
                <label for=\"address\">Адрес</label>
                <input style=\"margin-top:8px;\" type=\"text\" name=\"address\" id=\"address\" list=\"address_list\"
                       onkeyup=\"getAddress()\" autocomplete=\"off\">
                <datalist id=\"address_list\">
                </datalist>
                <label for=\"address\">Номер дома</label>
                <input style=\"margin-top:8px;\" type=\"text\" name=\"house\" id=\"house\" autocomplete=\"off\">
                <label for=\"address\">Квартира</label>
                <input style=\"margin-top:8px;\" type=\"text\" name=\"number_appart\" id=\"address\" autocomplete=\"off\">
            </div>
            </p>";
        }
        $str .= "
            <button type=\"submit\" class=\"btn btn-primary btn-block btn-large\" name=\"submit\" onclick=\"rowSec(this)\">Сохранить</button>
        </form>";
        return $str;
    }

    public function genForm($arr){
        $check = preg_match("/ідділення/",$arr['secession']);
        $str = "<h3>Номер накладної:{$arr['num_delivery']}</h3>
            <p>
                <label for=\"firstname\">Імя</label>
                <input style=\"margin-top:8px;\" type=\"text\" name=\"firstname\" id=\"firstname\"
                       value=\"{$arr['recipient_firstname']}\" readonly>
                <label for=\"surname\">Прізвище</label>
                <input style=\"margin-top:8px;\" type=\"text\" name=\"surname\" id=\"surname\"
                       value=\"{$arr['recipient_lastname']}\" readonly>
            </p>
            <p>
                <label for=\"city\">Місто</label>
                <input style=\"margin-top:8px;\" type=\"text\" name=\"city\" list=\"city_list\" id=\"city\" value=\"{$arr['city']}\" readonly>
            </p>
            <p>";

        if($check){
            $str .= "<label><input style=\"margin-right: 5px;\" name=\"type_del\" type=\"radio\"
                              value=\"type_secession\" readonly checked>Відділення</label>";
            $str.= "<label><input style=\"margin-left: 20px; margin-right: 5px;\" name=\"type_del\" type=\"radio\" value=\"type_address\" readonly>Адреса</label>
                ";
        }else{
            $str .= "<label><input style=\"margin-right: 5px;\" name=\"type_del\" type=\"radio\"
                              value=\"type_secession\" readonly>Відділення</label>";
            $str.= "<label><input style=\"margin-left: 20px; margin-right: 5px;\" name=\"type_del\" type=\"radio\" value=\"type_address\" readonly checked>Адреса</label>
                ";
        }
        if($check){
            $str .= "</p>
            <p>
            <div id=\"secession_div\">
                <label for=\"secession\">Відділення Нової Пошти</label>
                <input style=\"margin-top:8px;\" type=\"text\" name=\"secession\" list=\"secession_list\" id=\"secession\" value=\"{$arr['secession']}\" readonly>
            </div></p>";
        }else {
            $str .= "
            </p>
            <div id=\"address_div\">
                <label for=\"address\">Адреса</label>
                <input style=\"margin-top:8px;\" type=\"text\" name=\"address\" id=\"address\" value=\"{$arr['secession']}\" readonly>
            </div>
            </p>";
        }
        $str .= "<h3 style='color: white;' id='contact'>Для зміни даних зв'яжіться з оператором за телефонами: <br>Lifecell: +38(093)139-51-39<br>Kyivstar: +38(068)239-96-46</h3>";
        return $str;
    }

    public function getData($num_delivery)
    {
        $connect = new Database(HOST, DB, USER, PASS);
        $query = $connect->db->prepare('SELECT * FROM dispatch');
        $query->execute();
        $data = $query->fetchAll();
        foreach ($data as $key => $var) {
            if ($num_delivery === md5($var['inv_num_china'])) {
                $id = $var['inv_num_china'];
                $number = $var['inv_num_china'];
            }
        }
        if (!isset($id)) {
            return [
                "errors" => "Даних не знайдено"
            ];
        }
        try {
            $query = $connect->db->prepare('SELECT id_group FROM dispatch WHERE inv_num_china =' . $id);
            $query->execute();
            $data = $query->fetchAll();
        } catch (Exception $e) {
        }
        if (empty($data)) {
            return [
                "errors" => "Номер не найден"
            ];

        }
        $id_group = $data[0]['id_group'];
        $query = $connect->db->prepare('SELECT * FROM group_dispatch WHERE id =' . $id_group);
        $query->execute();
        $data = $query->fetchAll()[0];
        $query = $connect->db->prepare('SELECT inv_num_china,scan FROM dispatch WHERE id_group =' . $data['id']);
        $query->execute();
        $countInv = $query->fetchAll();
        $data['inv_track'] = $countInv;
        for ($i = 0; $i < count($countInv); $i++) {
            $countInv[$i] = $countInv[$i]['inv_num_china'];
        }
        $data['inv_china'] = $countInv;
        $countInv = $data['inv_track'];
        $del[] = null;
        for ($i = 0; $i < count($countInv); $i++) {
            if ($countInv[$i]['scan'] === null) {
                $countInv[$i] = $countInv[$i]['inv_num_china'];
            } else {
                $del[$i] = $i;
            }
        }
        foreach ($del as $key) {
            unset($countInv[$key]);
        }
        $data['inv_track'] = $countInv;
        $data['num_delivery'] = $number;
        return $data;
    }

}