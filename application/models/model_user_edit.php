<?php
/**
 * Created by PhpStorm.
 * User: Artemiy Karkusha
 * Date: 5/25/2018
 * Time: 10:28 AM
 */
class Model_user_edit extends Model{
    public function updateUser($array){
        try {
            $connect = new Database(HOST, DB, USER, PASS);
            $data = [
                'login' => $array['login'],
                'password' => $array['password'],
                'firstname' => $array['first_name'],
                'lastname' => $array['last_name'],
                'law' => $array['law']
            ];
            $sql = "UPDATE user SET login = :login,
                                password = :password,
                                firstname = :firstname,
                                lastname = :lastname,
                                law = :law
                    WHERE login = :login";
            $statement = $connect->db->prepare($sql);
            $statement->execute($data);
            return TRUE;
        }catch (Exception $e){
            return FALSE;
        }
    }
    public function unloadAllUser(){
    }
    public function generateCard(){
        $allCard = '';
        $connect = new Database(HOST, DB, USER, PASS);
        $sql = "SELECT * FROM user";
        $statement = $connect->db->prepare($sql);
        $statement->execute();
        $array = $statement->fetchAll();
        for($i = 0;$i < count($array);$i++){
            $data = $array[$i];
            $allCard .= ' <div class="row">
            <div class="col-md-12">
                <div class="box box-widget">
                    <form method="post" action="/user_edit">
                        <div class="row" style="margin-left: 10px;padding-top: 10px;margin-right: 10px;">
                            <div class="col-md-4 mb-3">
                                <label for="first_name">First name</label>
                                <input type="text" class="form-control" id="first_name" name="first_name" placeholder="First name" value="'.$data['firstname'].'" required>
                            </div>
                            <div class="col-md-4 mb-3">
                                <label for="last_name">Last name</label>
                                <input type="text" class="form-control" id="last_name" name="last_name" placeholder="Last name" value="'.$data['lastname'].'" required>
                            </div>
                            <div class="col-md-4 mb-3">
                                <label for="login">Login</label>
                                <input type="text" class="form-control" id="login" name="login" placeholder="Username" value="'.$data['login'].'" aria-describedby="inputGroupPrepend" required>
                            </div>
                        </div>
                        <div class="row" style="margin: 10px">
                            <div class="col-md-6 mb-3">
                                <label for="password">Password</label>
                                <input type="text" class="form-control" id="password" name="password" value="'.$data['password'].'" placeholder="Password">
                            </div>
                            <div class="col-md-3 mb-3">
                                <label for="law">Law</label>
                                <select class="form-control" id="law" name="law" required="">
                                    <option value="level_2" ';
            if($data['law'] == 'level_2')   $allCard .='selected';
            $allCard .='>User</option>
                                    <option value="level_3" ';
            if($data['law'] == 'level_3')   $allCard .='selected';
            $allCard .='>Admin</option>
                                </select>
                            </div>
                        </div>
                        <button class="btn btn-primary mt-10" type="submit" name="update_user" style="margin-left: 23px;margin-bottom: 10px">Update</button>
                        <button class="btn btn-primary mt-10 ml-10" type="submit" name="delete_user" style="margin-left: 23px;margin-bottom: 10px">Delete</button>
                    </form>
                </div>
            </div>
            </div>';
            unset($data);
        }
        return $allCard;
    }

    public function deleteUser($array){
        try {
            $connect = new Database(HOST, DB, USER, PASS);
            $data = [
                'login' => $array['login'],
            ];
            $sql = "DELETE FROM user WHERE login =  :login";
            $statement = $connect->db->prepare($sql);
            $statement->execute($data);
            return TRUE;
        }catch (Exception $e){
            return FALSE;
        }
    }
}