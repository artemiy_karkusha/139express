<?php
/**
 * Created by PhpStorm.
 * User: artemiy
 * Date: 8/14/2018
 * Time: 6:11 PM
 */

class model_notification extends Model
{

    public function getTableNotSaveForm(){
        $str = "";
        $connect = new Database(HOST, DB, USER, PASS);
        $query = $connect->db->prepare('SELECT * FROM group_dispatch WHERE form_save IS NULL and send_sms = 1 ORDER BY id');
        $query->execute();
        $not_save = $query->fetchAll();
        if(empty($not_save)){
            return false;
        }
        foreach ($not_save as $key => $value){
            $query = $connect->db->prepare('SELECT * FROM dispatch WHERE id_group='.$value['id']);
            $query->execute();
            $data = $query->fetchAll();
            $number_delivery = md5($data[0]['inv_num_china']);
            $row = $key + 1;
            $str .= "<tr>
                            <th scope=\"row\">$row</th>
                            <td>{$value['recipient_firstname']} {$value['recipient_lastname']}</td>
                            <td>{$value['phone']}</td>
                            <td>
                                <a class=\"btn btn-primary\" href=\"http://139express.loc/delivery?d={$number_delivery}\">Заполнить</a>
                            </td>
                        </tr>
                ";
        }
        return $str;
    }

    private function setSmsSend($number,$code = 1){
        $connect = new Database(HOST, DB, USER, PASS);
        $query = $connect->db->prepare('SELECT id_group FROM dispatch WHERE inv_num_china = "' . $number . '"');
        $query->execute();
        $dispatch = $query->fetchAll();
        $data = [
            'send_sms' => $code,
            'id' => $dispatch[0]['id_group']
        ];
        $sql = "UPDATE group_dispatch SET send_sms = :send_sms
                    WHERE id = :id";
        $statement = $connect->db->prepare($sql);
        try {
            $statement->execute($data);
        }catch (Exception $e){
        }
    }

    //Получить все телефоны на уоторые не было отправлено смс
    private function getPhones()
    {
        $arr = [];
        $connect = new Database(HOST, DB, USER, PASS);
        $query = $connect->db->prepare('SELECT * FROM group_dispatch WHERE send_sms IS NULL');
        $query->execute();
        $data = $query->fetchAll();
        if(empty($data)){
            return [
                "errors" => "Телефонов для отправки нету"
            ];
        }
        foreach ($data as $key => $value) {
            if(strlen($value['phone']) < 12){
                if($value['phone']{0} == "8"){
                    $value['phone'] = "3" . $value['phone'];
                }
                if($value['phone']{0} == "0"){
                    $value['phone'] = "38" . $value['phone'];
                }
                if(strlen($value['phone']) == 9){
                    $value['phone'] = "380" . $value['phone'];
                }
            }
            $arr[$key]['phone'] = $value['phone'];
            $query = $connect->db->prepare('SELECT num_delivery,inv_num_china FROM dispatch WHERE id_group=' . $value['id']);
            $query->execute();
            $dispatch = $query->fetchAll();
            $arr[$key]['inv_num_china'] =  $dispatch[0]['inv_num_china'];
            $arr[$key]['num_delivery'] =  $dispatch[0]['num_delivery'];
        }
        return $arr;
    }

    public function send($login = "artem.kar18@gmail.com", $password = "aae070341d", $sender = "139Express")
    {
        $data = $this->getPhones();
        if(isset($data['errors'])){
            return [
                "error" => "Телефонов для отправки нету"
            ];
        }
        foreach ($data as $key => $value){
            $message = "Vash zakaz {$value['inv_num_china']} postupil v Kiev, viberite adres dostavki http://139express.zzz.com.ua/delivery?d=" . md5($value['inv_num_china']);
            $src = '<?xml version="1.0" encoding="UTF-8"?>';
            $src .= "<SMS> 
            <operations>  
                <operation>SEND</operation> 
            </operations> 
            <authentification>    
                <username>$login</username>   
                <password>$password</password>   
            </authentification>   
            <message> 
                <sender>$sender</sender>    
                <text>$message</text>   
            </message>    
            <numbers>
                <number>{$value['phone']}</number>
            </numbers>    
        </SMS>";
            $curl = curl_init();
            $curlOptions = array(
                CURLOPT_URL => 'http://api.atompark.com/members/sms/xml.php',
                CURLOPT_FOLLOWLOCATION => false,
                CURLOPT_POST => true,
                CURLOPT_HEADER => false,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_CONNECTTIMEOUT => 15,
                CURLOPT_TIMEOUT => 100,
                CURLOPT_POSTFIELDS => array('XML' => $src),
            );
            curl_setopt_array($curl, $curlOptions);
            if (false === ($xmlString = curl_exec($curl))) {
                throw new Exception('Http request failed');
            }

            curl_close($curl);

            $xml = simplexml_load_string($xmlString);
            $json = json_encode($xml);
            $result = json_decode($json, 1);
            $this->setSmsSend($value['inv_num_china'],$result['status']);
        }
        return true;
    }
}