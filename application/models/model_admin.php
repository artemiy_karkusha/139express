<?php

/**
 * Created by PhpStorm.
 * User: Artemiy Karkusha
 * Date: 6/1/2018
 * Time: 12:52 PM
 */
class model_admin extends Model
{

    public function address()//db5c897c-391c-11dd-90d9-001a92567626
    {
        $connect = new Database(HOST, DB, USER, PASS);
        $query = $connect->db->prepare('SELECT * FROM city');
        $query->execute();
        $data = $query->fetchAll();
        foreach ($data as $key => $value) {
            $str = "
        {
            \"apiKey\": \"df728330f70c4af25d66d9f8b1a5a191\",
            \"modelName\": \"Address\",
            \"calledMethod\": \"getStreet\",
            \"methodProperties\": {
                \"CityRef\":\"{$value['ref']}\"
            }
        }
        ";
            $curl = curl_init('http://testapi.novaposhta.ua/v2.0/json/Address/getStreet/');
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($curl, CURLOPT_HTTPHEADER, [
                'Content-Type: application/json'
            ]);
            curl_setopt($curl, CURLOPT_POST, 1);
            curl_setopt($curl, CURLOPT_POSTFIELDS, $str);
            $result = curl_exec($curl);
            $result = json_decode($result, true);
            $data_res = $result['data'];
                foreach ($data_res as $ki => $val) {
                    $statement = $connect->db->prepare('INSERT INTO address(description, ref, streets_type, id_city) VALUES (:description, :ref, :streets_type, :id_city)');
                    $statement->bindParam(':description', $val['Description']);
                    $statement->bindParam(':ref', $val['Ref']);
                    $statement->bindParam(':streets_type', $val['StreetsType']);
                    $statement->bindParam(':id_city', $value['id']);
                    try {
                        $statement->execute();
                    } catch (Exception $e) {
                        echo $e->getMessage() . "<br>";
                    }
                }
            curl_close($curl);
            unset($data_res);
        }
        echo "Good all";
        exit;
    }

    public function downloadExcel($filename = 'example.xlsx', $mimetype = 'application/octet-stream')
    {
        if (file_exists($filename)) {
            header($_SERVER["SERVER_PROTOCOL"] . ' 200 OK');
            header('Content-Type: ' . $mimetype);
            header('Last-Modified: ' . gmdate('r', filemtime($filename)));
            header('ETag: ' . sprintf('%x-%x-%x', fileinode($filename), filesize($filename), filemtime($filename)));
            header('Content-Length: ' . (filesize($filename)));
            header('Connection: close');
            header('Content-Disposition: attachment; filename="' . basename($filename) . '";');
            // Открываем искомый файл
            $f = fopen($filename, 'r');
            while (!feof($f)) {
                // Читаем килобайтный блок, отдаем его в вывод и сбрасываем в буфер
                echo fread($f, 1024);
                flush();
            }
            // Закрываем файл
            fclose($f);
        } else {
            header($_SERVER["SERVER_PROTOCOL"] . ' 404 Not Found');
            header('Status: 404 Not Found');
        }
        exit;
    }
}