<?php
/**
 * Created by PhpStorm.
 * User: artem
 * Date: 7/23/2018
 * Time: 2:42 PM
 */

use NovaPoshta\Config;
use NovaPoshta\ApiModels\InternetDocument;
use NovaPoshta\ApiModels\Address;

class model_notcollected extends Model
{

    public $arrStatus;

    public function __construct()
    {
        Config::setApiKey('df728330f70c4af25d66d9f8b1a5a191');
        Config::setFormat(Config::FORMAT_JSONRPC2);
        Config::setLanguage(Config::LANGUAGE_UA);
    }

    protected function genTable($arr){
        $inv_number_china = '';
        $table = '';
        for($i = 0; $i < count($arr); $i++) {
            foreach ($arr[$i]['china'] as $key => $value) {
                $inv_number_china .= $value .";\n";
            }
            $table .= "<tr>
                                    <td>{$arr[$i]['number']}</td>
                                    <td>{$inv_number_china}
                                    </td>
                                    <td>{$arr[$i]['countDelivery']}</td>
                                    <td>{$arr[$i]['dateScan']}</td>
                                    <td>{$arr[$i]['status']}</td>
                                </tr>";
            unset($inv_number_china);
        }
        return $table;
    }




    public function getCity($index){
        $str = "
        {
            \"apiKey\": \"df728330f70c4af25d66d9f8b1a5a191\",
            \"modelName\": \"Address\",
            \"calledMethod\": \"searchSettlements\",
            \"methodProperties\": {
                \"CityName\":\"{$index}\"
            }
        }
        ";
        $curl = curl_init('http://testapi.novaposhta.ua/v2.0/json/Address/searchSettlements');
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_HTTPHEADER, [
            'Content-Type: application/json'
        ]);
        curl_setopt($curl, CURLOPT_POST, 1);
        //curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($group));
        curl_setopt($curl, CURLOPT_POSTFIELDS, $str);
        $result = curl_exec($curl);
        $result = json_decode($result, true);
        curl_close($curl);
        return $result['data'][0]['Addresses'][0]['MainDescription'];
    }

    public function getAllWarehouse($city){
        $str = "
        {
            \"apiKey\": \"df728330f70c4af25d66d9f8b1a5a191\",
            \"modelName\": \"AddressGeneral\",
            \"calledMethod\": \"getWarehouses\",
            \"methodProperties\": {
                \"CityName\":\"{$city}\"
            }
        }
        ";
        $curl = curl_init('http://testapi.novaposhta.ua/v2.0/json/AddressGeneral/getWarehouses');
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_HTTPHEADER, [
            'Content-Type: application/json'
        ]);
        curl_setopt($curl, CURLOPT_POST, 1);
        //curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($group));
        curl_setopt($curl, CURLOPT_POSTFIELDS, $str);
        $result = curl_exec($curl);
        $result = json_decode($result, true);
        curl_close($curl);
        if(empty($result['data'])){
            return [
              "errors" => "Отделений в населённом пункте нету"
            ];
        }else{
            return $result['data'];
        }
    }

    public function getStatus()
    {
        $connect = new Database(HOST, DB, USER, PASS);
        $query = $connect->db->prepare('SELECT * FROM group_dispatch WHERE scan = 1');
        $query->execute();
        $data = $query->fetchAll();
        foreach ($data as $key => $value) {
            $query = $connect->db->prepare('SELECT inv_num_china,scan FROM dispatch WHERE id_group =' . $data[$key]['id']);
            $query->execute();
            $countInv = $query->fetchAll();
            for ($i = 0; $i < count($countInv); $i++) {
                $countInv[$i] = $countInv[$i]['inv_num_china'];
            }
            $data[$key]['inv_china'] = $countInv;
            $str = "
        {
            \"apiKey\": \"df728330f70c4af25d66d9f8b1a5a191\",
            \"modelName\": \"TrackingDocument\",
            \"calledMethod\": \"getStatusDocuments\",
            \"methodProperties\": {
                \"Documents\": [
                    {
                     \"DocumentNumber\": \"";
            $str .= $data[$key]['inv_num_np'] . "\",
                        \"Phone\":\"\"
                     }
                 ]
            }
        }
        ";
            $curl = curl_init('http://testapi.novaposhta.ua/v2.0/en/documentsTracking/json/');
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($curl, CURLOPT_HTTPHEADER, [
                'Content-Type: application/json'
            ]);
            curl_setopt($curl, CURLOPT_POST, 1);
            //curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($group));
            curl_setopt($curl, CURLOPT_POSTFIELDS, $str);
            $result = curl_exec($curl);
            $result = json_decode($result, true);
            curl_close($curl);
            //debug($result);
            $arrStatus[$key] = [
                "number" => $result['data'][0]['Number'],
                "china" => $data[$key]['inv_china'],
                "countDelivery" => $data[$key]['count_delivery'],
                "dateScan" => $result['data'][0]['DateScan'],
                "status" => $result['data'][0]['Status'],
            ];
        }
        $result = $this->genTable($arrStatus);

        return $result;
    }

}