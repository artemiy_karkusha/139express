<?php
/**
 * Created by PhpStorm.
 * User: Artemiy Karkusha
 * Date: 5/24/2018
 * Time: 12:15 PM
 */

class model_delivery_excel extends Model
{
    private $excel = [
        'delivery number' => [],
        'delivery shop' => [],
        'client name' => [],
        'phone' => [],
        'city' => [],
        'secession' => [],
        'weight' => [],
        'cost' => [],
    ];

    public $countAddRow = 0;
    private $connect;
    public $count_delivery = 0;

    public function __construct()
    {
        $this->connect = new Database(HOST, DB, USER, PASS);
    }


    protected function getData($select)
    {
        $connect = new Database(HOST, DB, USER, PASS);
        $query = $connect->db->prepare('SELECT id,inv_num_np,delivery_time,shipping_cost FROM group_dispatch WHERE scan = 1 and name_file="' . $select . '"');
        $query->execute();
        $data = $query->fetchAll();
        $str = '';
        for ($i = 0; $i < count($data); $i++) {
            $query = $connect->db->prepare('SELECT inv_num_china FROM dispatch WHERE id_group = ' . $data[$i]['id']);
            $query->execute();
            $countInv = $query->fetchAll();
            for ($j = 0; $j < count($countInv); $j++) {
                $str .= $countInv[$j]['inv_num_china'] . ";";
                //$countInv[$j] = $countInv[$j]['inv_num_china'];
            }
            $data[$i]['inv_china'] = $str;
            unset($str);
        }
        return $data;
    }

    public function getDataDelivety($id)
    {
        $connect = new Database(HOST, DB, USER, PASS);
        $query = $connect->db->prepare('SELECT * FROM dispatch WHERE inv_num_china =' . $id);
        $query->execute();
        $data = $query->fetchAll();
        return $data;
    }

    public function reportExcel()
    {
        $data = $this->getData($_POST['select']);
        if (empty($data)) {
            return [
                'errors' => 'Еще не оформленно не одной доставки'
            ];
        }
        //Класс для чтения
        try {
            require './application/core/Classes/PHPExcel.php';
            //Класс для работы с датами в Excel
            require './application/core/Classes/PHPExcel/Writer/Excel5.php';
        } catch (Exception $e) {
            echo $e->getMessage();
        }
        $border = [
            'borders' => [
                'bottom' => [
                    'style' => PHPExcel_Style_Border::BORDER_MEDIUM,
                    'color' => [
                        'rgb' => '808080'
                    ]
                ],
                'top' => [
                    'style' => PHPExcel_Style_Border::BORDER_MEDIUM,
                    'color' => [
                        'rgb' => '808080'
                    ]
                ],
                'left' => [
                    'style' => PHPExcel_Style_Border::BORDER_MEDIUM,
                    'color' => [
                        'rgb' => '808080'
                    ]
                ],
                'right' => [
                    'style' => PHPExcel_Style_Border::BORDER_MEDIUM,
                    'color' => [
                        'rgb' => '808080'
                    ]
                ]
            ]
        ];
        //Массив с настройками для выравнивания текста в ячейках
        $alignmentCenter = [
            'alignment' => [
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER
            ]
        ];
        $alignmentCenterVertical = [
            'alignment' => [
                'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER
            ]
        ];

        $colorRed = [
            'fill' => [
                'type' => 'solid',
                'color' => [
                    'rgb' => 'FF0000'
                ]
            ]
        ];
        $colorGreen = [
            'fill' => [
                'type' => 'solid',
                'color' => [
                    'rgb' => '00CD0E'
                ]
            ]
        ];
        $colorGrey = [
            'fill' => [
                'type' => 'solid',
                'color' => [
                    'rgb' => '8E91EF'
                ]
            ]
        ];
        $colorCommon = [
            'fill' => [
                'type' => 'solid',
                'color' => [
                    'rgb' => 'E4E4E4'
                ]
            ]
        ];
        $fontTop = [
            'font' => [
                'name' => 'Arial',
                'size' => 10,
                'bold' => true
            ]
        ];
        // Создаем объект класса PHPExcel
        $xls = new PHPExcel();
        // Устанавливаем индекс активного листа
        $xls->setActiveSheetIndex(0);
        // Получаем активный лист
        $sheet = $xls->getActiveSheet();
        // Подписываем лист
        $sheet->setTitle('Report');
        // Устанавливаем название и оформление столбцов
        $sheet->setCellValue("A1", 'Number China');
        $sheet->getColumnDimension('A')->setWidth(30);
        // Выравнивание текста
        $sheet->getStyle('A1')->applyFromArray($alignmentCenter);
        $sheet->setCellValue("B1", 'Number NovaPoshta');
        $sheet->getColumnDimension('B')->setWidth(30);
        $sheet->setCellValue("C1", 'Cost');
        $sheet->getColumnDimension('С')->setWidth(20);
        // Выравнивание текста
        $sheet->getStyle('C1')->applyFromArray($alignmentCenter);
        $sheet->setCellValue("D1", 'Arrival date');
        $sheet->getStyle('D1')->applyFromArray($alignmentCenter);
        $sheet->getColumnDimension('D')->setWidth(30);
        for ($i = 0; $i < count($data); $i++) {
            $j = $i + 2;
            $sheet->setCellValue("A" . $j, $data[$i]['inv_china']);
            $sheet->getStyle('A' . $j)->applyFromArray($alignmentCenter);
            $sheet->setCellValue("B" . $j, $data[$i]['inv_num_np']);
            $sheet->getStyle('B' . $j)->applyFromArray($alignmentCenter);
            $sheet->setCellValue("C" . $j, $data[$i]['shipping_cost']);
            $sheet->getStyle('C' . $j)->applyFromArray($alignmentCenter);
            $sheet->setCellValue("D" . $j, $data[$i]['delivery_time']);
            $sheet->getStyle('D' . $j)->applyFromArray($alignmentCenter);
        }
        //Рамки
        for ($i = 0; $i < 4; $i++) {
            for ($j = 0; $j < count($data) + 2; $j++) {
                $sheet->getStyleByColumnAndRow($i, $j)->applyFromArray($border);
            }
        }
        date_default_timezone_set('Europe/Kiev');
        $date_time = date("Y-m-d H:i:s");
        $objWriter = new PHPExcel_Writer_Excel5($xls);
        header('Content-type: application/vnd.ms-excel;charset=utf-8');
        header('Content-Disposition: attachment; filename="report-' . $date_time . '.xls"');
        $objWriter->save('php://output');
        exit;
    }


    public function addFromOldExcel()
    {
        //Проверяем наличие файла во временной деректории
        if (isset($_FILES['upload']['tmp_name'])) {
            $uploadInfo = $_FILES['upload'];
            //Проверяем форма передаваемого файла
            if ($uploadInfo['type'] == 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' or $uploadInfo['type'] == 'application/vnd.ms-excel' or $uploadInfo['type'] == 'application/octet-stream') {
                //Создаем подключенение к базе данных используя класс Database
                $connect = new Database(HOST, DB, USER, PASS);
                //Класс для чтения
                try {
                    require './application/core/Classes/PHPExcel/IOFactory.php';
                    //Класс для работы с датами в Excel
                    require './application/core/Classes/PHPExcel/Shared/Date.php';
                } catch (Exception $e) {
                    //echo $e->getMessage();
                }
                $xls = PHPExcel_IOFactory::load($uploadInfo['tmp_name']);
                // Устанавливаем индекс активного листа
                $xls->setActiveSheetIndex(0);

                // Получаем активный лист
                $sheet = $xls->getActiveSheet();
                //count row on sheet
                $countRow = $sheet->getHighestRow();
                //count column on sheet
                $countColumn = PHPExcel_Cell::columnIndexFromString($sheet->getHighestColumn());
                ////Достаем название каждой колонке
                for ($i = 0; $i < $countColumn; $i++) {
                    //значение колонки
                    $valueColumn = $sheet->getCellByColumnAndRow($i, 1)->getValue();
                    //$valueColumn = mb_strtolower($this->translate((string)$valueColumn, 'zh-CN', 'ru'));
                    //сравниваем полученое значение колонки со значениями в свойстве excel
                    foreach ($this->excel as $key => $value) {

                        if (preg_match('/' . $key . '/', $valueColumn)) {
                            //Если совпадение то начинаем проходится по рядку
                            for ($j = 2; $j < $countRow + 1; $j++) {
                                //Получаем значение ячейки рядка в колонке $i
                                $data = $sheet->getCellByColumnAndRow($i, $j)->getValue();
                                //Если значение этого елемента равно условию выполняем дополнительную обработку
                                //Используем класс Date для получения с ячейки даты
                                //Если значение этого елемента равно условию выполняем дополнительную обработку
                                //Добавляем в массив значение ячеки рядка
                                /*if ($valueColumn == 'получатель коробки') {
                                    echo $valueColumn;
                                    if ($data == '') {
                                        $data = $sheet->getCellByColumnAndRow($i, $j - 1)->getValue();
                                    }
                                }*/
                                if (!empty($data)) {
                                    $value [] = $data;
                                }
                            }
                            //После прохождение всех рядков присваиваем заполнений масив $value в свойство класс с ключем $key
                            $this->excel[$key] = $value;
                        }
                    }
                }
                //Заносим в базу данных
                //Начинаем собирать запрос
                //Проходимся по первому елементу в каждом массиве
                for ($i = 0; $i < count($this->excel['delivery number']); $i++) {
                    //Запрос для таблицы dispatch
                    $sqlDispatch = "INSERT INTO dispatch(";

                    foreach ($this->excel as $key => $value) {
                        if (isset($value[$i])) {
                            //Добавляем к запросу колонки при наличи ключей в свойстве
                            switch ($key) {
                                case 'delivery number': {
                                    $sqlDispatch .= 'inv_num_china,';
                                    break;//
                                }
                                case 'delivery shop': {
                                    $sqlDispatch .= 'num_delivery,';
                                    break;
                                }
                                case 'client name': {
                                    $sqlDispatch .= 'first_name,';
                                    $sqlDispatch .= 'surname,';
                                    break;
                                }

                                case 'phone': {
                                    $sqlDispatch .= 'phone,';
                                    break;
                                }
                                case 'city': {
                                    $sqlDispatch .= 'city,';
                                    break;
                                }
                                case 'secession': {
                                    $sqlDispatch .= 'secession,';
                                    break;
                                }
                                case 'weight': {
                                    $sqlDispatch .= 'weight,';
                                    break;
                                }
                                case 'cost': {
                                    $sqlDispatch .= 'shipping_cost,';
                                    break;
                                }
                            }
                        }
                    }
                    //убераем кому
                    for ($str = 0; $str < strlen($sqlDispatch); $str++) {

                        if ($str == (strlen($sqlDispatch) - 1)) {
                            //Если последняя не кома значит значений для заполнения нету
                            if ($sqlDispatch{$str} != ',') {
                                unset($sqlDispatch);
                            }
                            if ($sqlDispatch{$str} == ',') {
                                $sqlDispatch = substr($sqlDispatch, 0, $str);
                            }
                        }
                    }

                    if (isset($sqlDispatch)) {
                        $sqlDispatch .= ',name_file) VALUES(';
                    }
                    foreach ($this->excel as $key => $value) {
                        if (isset($value[$i])) {
                            //Добавляем к запросу колонки при наличи ключей в свойстве
                            switch ($key) {
                                case 'delivery number': {
                                    $sqlDispatch .= ':inv_num_china,';
                                    break;
                                }//
                                case 'delivery shop': {
                                    $sqlDispatch .= ':num_delivery,';
                                    break;
                                }
                                case 'client name': {
                                    $sqlDispatch .= ':first_name,';
                                    $sqlDispatch .= ':surname,';
                                    break;
                                }
                                case 'phone': {
                                    $sqlDispatch .= ':phone,';
                                    break;
                                }
                                case 'city': {
                                    $sqlDispatch .= ':city,';
                                    break;
                                }
                                case 'secession': {
                                    $sqlDispatch .= ':secession,';
                                    break;
                                }
                                case 'weight': {
                                    $sqlDispatch .= ':weight,';
                                    break;
                                }
                                case 'cost': {
                                    $sqlDispatch .= ':shipping_cost,';
                                    break;
                                }
                            }
                        }
                    }
                    //убераем кому
                    for ($str = 0; $str < strlen($sqlDispatch); $str++) {

                        if ($str == (strlen($sqlDispatch) - 1)) {
                            if ($sqlDispatch{$str} == ',') {
                                $sqlDispatch = substr($sqlDispatch, 0, $str);
                            }
                        }
                    }
                    if (isset($sqlDispatch)) {
                        $sqlDispatch .= ',:name_file)';
                    }
                    $statement = $connect->db->prepare($sqlDispatch);
                    foreach ($this->excel as $key => $value) {
                        if (isset($value[$i])) {
                            switch ($key) {
                                case 'delivery number': {
                                    $infoDelivery = $this->getDataDelivety(trim($value[$i]));
                                    $statement->bindParam(':inv_num_china', trim($value[$i]));
                                    break;
                                }
                                case 'delivery shop': {
                                    $statement->bindParam(':num_delivery', trim($value[$i]));
                                    break;
                                }
                                case 'client name': {
                                    $val = explode(" ", trim($value[$i]));
                                    $statement->bindParam(':first_name', $val[1]);
                                    $statement->bindParam(':surname', $val[0]);
                                    break;
                                }
                                case 'phone': {
                                    $phone = trim($value[$i]);
                                    if ($phone{0} != 0) {
                                        $phone = 0 . $phone;
                                    }
                                    $statement->bindParam(':phone', $phone);
                                    break;
                                }
                                case 'secession': {
                                    $statement->bindParam(':secession', trim($value[$i]));
                                    break;
                                }
                                case 'city': {
                                    $statement->bindParam(':city', trim($value[$i]));
                                    break;
                                }
                                case 'weight': {
                                    $statement->bindParam(':weight', trim($value[$i]));
                                    break;
                                }
                                case 'cost': {
                                    $statement->bindParam(':shipping_cost', trim($value[$i]));
                                    break;
                                }
                            }
                        }
                    }
                    try {
                        if (!empty($infoDelivery)) {
                            unset($statement);
                        } else {
                            $statement->bindParam(':name_file', $_FILES['upload']['name']);
                            $statement->execute();
                            $this->countAddRow++;
                            unset($statement);
                        }
                    } catch (Exception $e) {
                        //echo 'Выброшено исключение: ', $e->getMessage(), "\n";
                        $this->countAddRow = $i;
                        return FALSE;
                    }
                }
                return TRUE;
            }
            return 'Type not exist';
        }
        return NULL;
    }

    private function takeSec($phone, $firstname, $surname)
    {
        $connect = new Database(HOST, DB, USER, PASS);
        $query = $connect->db->prepare('SELECT * FROM group_dispatch WHERE phone="' . $phone . '" and inv_num_np IS NOT NULL');
        $query->execute();
        $data = $query->fetchAll();
        if (empty($data)) {
            return false;
        } else {
            return [
                "city" => $data[0]['city'],
                "secession" => $data[0]['secession']
            ];
        }
    }


    public function changeTable()
    {
        $connect = new Database(HOST, DB, USER, PASS);
        $query = $connect->db->prepare('SELECT * FROM dispatch WHERE id_group IS NULL');
        $query->execute();
        $data = $query->fetchAll();
        $coincidence = 0;
        if (!empty($data)) {
            $count = count($data);
            for ($i = 0; $i < $count; $i++) {
                if (isset($data[$i]) === true) {
                    if ($i == $count - 1) {
                        $coincidence = 0;
                    } else {
                        $phone = $data[$i]['phone'];
                        for ($j = $i + 1; $j < $count; $j++) {
                            if ($phone == $data[$j]['phone']) {
                                $coincidence++;
                                $array [] = $j;
                            }
                        }
                        $array [] = $i;
                    }
                    if (!$coincidence) {
                        $connect = new Database(HOST, DB, USER, PASS);
                        $first_name = $data[$i]['first_name'];
                        $surname = $data[$i]['surname'];
                        $city = $data[$i]['city'];
                        if ($data[$i]['weight'] <= 0.5) {
                            $round = 0.5;
                        }
                        if ($data[$i]['weight'] > 0.5 && $data[$i]['weight'] <= 1) {
                            $round = 1;
                        }
                        if ($data[$i]['weight'] > 1) {
                            $round = ceil($data[$i]['weight']);
                        }
                        $takeSec = $this->takeSec($data[$i]['phone'], $first_name, $surname);

                        if ($takeSec !== false) {
                            $city = $takeSec['city'];
                            $data[$i]['secession'] = $takeSec['secession'];
                        } else {
                            $city = null;
                            $data[$i]['secession'] = null;
                        }
                        if (!isset($city)) {
                            $statement = $connect->db->prepare("INSERT INTO group_dispatch(recipient_firstname, recipient_lastname, city, phone, secession,price,width, name_file) VALUES (:recipient_firstname, :recipient_lastname, :city, :phone, :secession,:price,:width,:name_file)");
                            $statement->bindParam(':recipient_firstname', $first_name);
                            $statement->bindParam(':recipient_lastname', $surname);
                            $statement->bindParam(':city', $city);
                            $statement->bindParam(':phone', $data[$i]['phone']);
                            $statement->bindParam(':secession', $data[$i]['secession']);
                            $statement->bindParam(':price', $data[$i]['shipping_cost']);
                            $statement->bindParam(':width', $round);
                        } else {
                            $one = 1;
                            $statement = $connect->db->prepare("INSERT INTO group_dispatch(recipient_firstname, recipient_lastname, city, phone, secession,price,width,send_sms,form_save,name_file) VALUES (:recipient_firstname, :recipient_lastname, :city, :phone, :secession,:price,:width,:send_sms,:form_save,:name_file)");
                            $statement->bindParam(':recipient_firstname', $first_name);
                            $statement->bindParam(':recipient_lastname', $surname);
                            $statement->bindParam(':city', $city);
                            $statement->bindParam(':phone', $data[$i]['phone']);
                            $statement->bindParam(':secession', $data[$i]['secession']);
                            $statement->bindParam(':price', $data[$i]['shipping_cost']);
                            $statement->bindParam(':width', $round);//send_sms
                            $statement->bindParam(':send_sms', $one);
                            $statement->bindParam(':form_save', $one);
                        }
                        try {
                            $statement->bindParam(':name_file', $data[$i]['name_file']);//send_sms
                            $statement->execute();
                            $id_dispatch = $connect->db->lastInsertId();
                            unset($statement);
                            $sql = "UPDATE dispatch SET id_group = :id_group WHERE id = :id";
                            $stat = $connect->db->prepare($sql);
                            $stat->bindParam(':id_group', $id_dispatch);
                            $stat->bindParam(':id', $data[$i]['id']);
                            $tack = $stat->execute();
                            $this->count_delivery++;
                            unset($statement);
                            $coincidence = 0;
                            unset($data[$i]);
                            unset($array);
                        } catch (Exception $e) {
                            return false;
                        }
                    } else {
                        $query = $connect->db->prepare('SELECT * FROM dispatch WHERE phone = "' . $data[$i]['phone'] . '" and id_group IS NULL');
                        $query->execute();
                        $many = $query->fetchAll();
                        unset($query);
                        $weight = 0;
                        $price = 0;
                        for ($k = 0; $k < count($many); $k++) {
                            $weight += (float)$many[$k]['weight'];
                            $price += $many[$k]['shipping_cost'];
                        }
                        if ($weight <= 0.5) {
                            $weight = 0.5;
                        }
                        if ($weight > 0.5 && $weight <= 1) {
                            $weight = 1;
                        }
                        if ($weight > 1) {
                            $weight = ceil($weight);
                        }
                        $connect = new Database(HOST, DB, USER, PASS);
                        $first_name = $data[$i]['first_name'];
                        $surname = $data[$i]['surname'];
                        $takeSec = $this->takeSec($many[0]['phone'], $first_name, $surname);
                        if ($takeSec !== false) {
                            $city = $takeSec['city'];
                            $many[0]['secession'] = $takeSec['secession'];
                        } else {
                            $city = null;
                            $many[0]['secession'] = null;
                        }
                        $count_delivery = count($array);
                        if (!isset($city)) {
                            $statement = $connect->db->prepare("INSERT INTO group_dispatch(recipient_firstname, recipient_lastname, city, phone, secession,price,width,count_delivery, name_file) VALUES (:recipient_firstname, :recipient_lastname, :city, :phone, :secession,:price,:width,:count_delivery, :name_file)");
                            $statement->bindParam(':recipient_firstname', $first_name);
                            $statement->bindParam(':recipient_lastname', $surname);
                            $statement->bindParam(':city', $city);
                            $statement->bindParam(':phone', $many[0]['phone']);
                            $statement->bindParam(':secession', $many[0]['secession']);
                            $statement->bindParam(':price', $price);
                            $statement->bindParam(':width', $weight);
                            $statement->bindParam(':count_delivery', $count_delivery);
                        } else {
                            $one = 1;
                            $statement = $connect->db->prepare("INSERT INTO group_dispatch(recipient_firstname, recipient_lastname, city, phone, secession,price,width,send_sms,form_save,count_delivery, name_file) VALUES (:recipient_firstname, :recipient_lastname, :city, :phone, :secession,:price,:width,:send_sms,:form_save,:count_delivery,:name_file)");
                            $statement->bindParam(':recipient_firstname', $first_name);
                            $statement->bindParam(':recipient_lastname', $surname);
                            $statement->bindParam(':city', $city);
                            $statement->bindParam(':phone', $many[0]['phone']);
                            $statement->bindParam(':secession', $many[0]['secession']);
                            $statement->bindParam(':price', $price);
                            $statement->bindParam(':width', $weight);
                            $statement->bindParam(':send_sms', $one);
                            $statement->bindParam(':form_save', $one);
                            $statement->bindParam(':count_delivery', $count_delivery);
                        }

                        try {
                            $statement->bindParam(':name_file', $many[0]['name_file']);
                            $statement->execute();
                            $id = $connect->db->lastInsertId();
                            for ($k = 0; $k < count($many); $k++) {
                                $arr = [
                                    'id_group' => $id,
                                    'id' => $many[$k]['id']
                                ];
                                $sql = "UPDATE dispatch SET id_group = :id_group WHERE id = :id";
                                $statement = $connect->db->prepare($sql);
                                $statement->execute($arr);
                                unset($statement);
                            }
                            unset($statement);
                            foreach ($array as $key => $value) {
                                unset($data[$value]);
                            }
                            unset($array);
                            $count_delivery = 0;
                            $this->count_delivery++;
                            $coincidence = 0;
                        } catch (Exception $e) {
                            echo $e->getMessage();
                            //return false;
                        }
                    }
                }
            }
        }
        return $this->count_delivery;
    }

    public function getInfoSelect()
    {
        try {
            $connect = new Database(HOST, DB, USER, PASS);
            $statement = $connect->db->prepare("SELECT name_file FROM group_dispatch WHERE name_file IS NOT NULL");
            $statement->execute();
            $query = $statement->fetchAll();
        } catch (Exception $e) {
            echo $e->getMessage();
        }
        $count = count($query);
        for ($i = 0; $i < $count; $i++) {
            if (isset($query[$i])) {
                for ($j = $i + 1; $j < $count; $j++) {
                    if ($query[$j] == $query[$i]) {
                        unset($query[$j]);
                    }
                }
            }
        }
        $str = "<select class=\"form-control select2 select2-hidden-accessible\" style=\"width: 50%;margin-top: 10px;\" tabindex=\"-1\" aria-hidden=\"true\" name=\"select\">
        <option selected=\"selected\">Выберете отчёт</option>";
        foreach ($query as $key => $value) {
            $str .= "<option value=\"{$value['name_file']}\">{$value['name_file']}</option>";
        }
        return $str;
    }
}


