<?php
/**
 * Created by PhpStorm.
 * User: Artemiy Karkusha
 * Date: 5/25/2018
 * Time: 10:17 AM
 */

use NovaPoshta\Config;

class model_novaposhta extends Model
{
    private $connect;


    public function __construct()
    {
        $this->connect = new Database(HOST, DB, USER, PASS);
        Config::setApiKey(ANP);
        Config::setFormat(Config::FORMAT_JSONRPC2);
        Config::setLanguage(Config::LANGUAGE_UA);
    }

    private function addIndex($index)
    {
        if (strlen($index) <= 4) {
            return '0' . $index;
        } else {
            return $index;
        }
    }

    public function setScan($id){
        try {
            $connect = new Database(HOST, DB, USER, PASS);
            $data = [
                'scan' => 1,
                'inv_num_china' => $id,
            ];
            $sql = "UPDATE dispatch SET scan = :scan
                    WHERE inv_num_china = :inv_num_china";
            $statement = $connect->db->prepare($sql);
            $statement->execute($data);
            return TRUE;
        } catch (Exception $e) {
            return FALSE;
        }
    }


    public function leftNum($arr){
        $body = '';
        foreach ($arr as $key => $value){
            $body .= '<h3>'.$value.'</h3>';
        }
        return $body;
    }


    public function getData($id)
    {
        $connect = new Database(HOST, DB, USER, PASS);
        $query = $connect->db->prepare('SELECT id_group FROM dispatch WHERE inv_num_china =' . $id);
        $query->execute();
        $data = $query->fetchAll();
        if (empty($data)) {
            return [
                'errors' => 'Номер накладной не найден'
            ];
        }
        $id_group = $data[0]['id_group'];
        $query = $connect->db->prepare('SELECT * FROM group_dispatch WHERE id =' . $id_group);
        $query->execute();
        $data = $query->fetchAll()[0];
        if ($data['price'] === null) {
            return [
                'errors' => 'Цена не задана'
            ];
        }
        $query = $connect->db->prepare('SELECT inv_num_china,scan FROM dispatch WHERE id_group =' . $data['id']);
        $query->execute();
        $countInv = $query->fetchAll();
        $data['inv_track'] = $countInv;
        for ($i = 0; $i < count($countInv); $i++) {
            $countInv[$i] = $countInv[$i]['inv_num_china'];
        }
        $data['inv_now'] = $id;
        $data['inv_china'] = $countInv;
        $countInv = $data['inv_track'];
        $del[] = null;
        for ($i = 0; $i < count($countInv); $i++) {
            if ($countInv[$i]['scan'] === null) {
                $countInv[$i] = $countInv[$i]['inv_num_china'];
            } else {
                $del[$i] = $i;
            }
        }
        foreach ($del as $key){
            unset($countInv[$key]);
        }
        $data['inv_track'] = $countInv;
        return $data;
    }

    private function warehouseSet($object, $arr)
    {
        $data = $object;
        if(strripos($arr['secession'], ":")){
            $arr = explode(":",$arr['secession']);
            if(strripos($arr[0],"(")){
                $arr = explode("(",$arr[0]);
                $arr = trim($arr[0]);
            }else{
                $arr = (string)trim($arr[0]);
            }
            for ($i = 0; $i < count($data); $i++) {
                if(count($data) == 1 ){
                    return $data[$i]['ref'];
                }
                if (preg_match("/$arr/", $data[$i]['description'])) {
                    return $data[$i]['ref'];
                }
            }
        }else{
            $arr = $arr['secession'];
            for ($i = 0; $i < count($data); $i++) {
                if(count($data) == 1 ){
                    return $data[$i]['ref'];
                }
                if (preg_match("/$arr/", $data[$i]['description'])) {
                    return $data[$i]['ref'];
                }
            }
        }

        return false;
    }

    protected function updateInfoDelivery($internetDocument, $link, $arr, $bool = true)
    {
        //CostOnSite стоимость доставки
        //Update link,cost shipping, barcode
        try {
            $connect = new Database(HOST, DB, USER, PASS);
            if($bool) {
                $data = [
                    'inv_num_np' => $internetDocument->data[0]->IntDocNumber,
                    'shipping_cost' => $internetDocument->data[0]->CostOnSite,
                    'ref' => $internetDocument->data[0]->Ref,
                    'delivery_time' => $internetDocument->data[0]->EstimatedDeliveryDate,
                    'link_print' => $link,
                    'scan' => 1,
                    'id' => $arr['id'],
                ];
            } else{
                $data = [
                    'inv_num_np' => $internetDocument['data'][0]['IntDocNumber'],
                    'shipping_cost' => $internetDocument['data'][0]['CostOnSite'],
                    'ref' => $internetDocument['data'][0]['Ref'],
                    'delivery_time' => $internetDocument['data'][0]['EstimatedDeliveryDate'],
                    'link_print' => $link,
                    'scan' => 1,
                    'id' => $arr['id'],
                ];
            }
            $sql = "UPDATE group_dispatch SET inv_num_np = :inv_num_np,
                                shipping_cost = :shipping_cost,
                                ref = :ref,
                                delivery_time = :delivery_time,
                                scan = :scan,
                                link_print = :link_print
                    WHERE id = :id";
            $statement = $connect->db->prepare($sql);
            $statement->execute($data);
            return TRUE;
        } catch (Exception $e) {
            return FALSE;
        }
    }

    public function addForwarding($arr)
    {
        $numberInv = '';
        for ($i = 0; $i < count($arr['inv_china']); $i++) {
            $numberInv .= $arr['inv_china'][$i] . ',';
        }
        //убераем кому
        for ($str = 0; $str < strlen($numberInv); $str++) {
            if ($str == (strlen($numberInv) - 1)) {
                if ($numberInv{$str} == ',') {
                    $numberInv = substr($numberInv, 0, $str);
                }
            }
        }
        //Выбираем город отправителя:
        $data = new \NovaPoshta\MethodParameters\Address_getCities();
        $data->setFindByString('Киев');
        $result = \NovaPoshta\ApiModels\Address::getCities($data);
        $citySender = $result->data[0]->Ref;

        //Выбираем город получателя:
        $data = new \NovaPoshta\MethodParameters\Address_getCities();
        $data->setFindByString($arr['city']);
        $result = \NovaPoshta\ApiModels\Address::getCities($data);
        $cityRecipient = $result->data[0]->Ref;
        if (empty($cityRecipient)) {
            return [
                "errors" => "Город не найден"
            ];
        }
        //Если нет контрагента отправителя в городе Киев, создаем там контрагента отправителя. Контрагент создастся через несколько минут:
        //Выбираем тип контрагента:
        $result = \NovaPoshta\ApiModels\Common::getTypesOfCounterparties();
        $counterpartyType = $result->data[1]->Ref;// Приватная


        //Создаем контрагента получателя:
        $counterparty = new \NovaPoshta\ApiModels\Counterparty();
        $counterparty->setCounterpartyProperty(\NovaPoshta\ApiModels\Counterparty::RECIPIENT);
        $counterparty->setCityRef($cityRecipient);
        $counterparty->setCounterpartyType($counterpartyType);
        $counterparty->setFirstName($arr['recipient_lastname']);
        $counterparty->setLastName($arr['recipient_firstname']);
        $counterparty->setPhone($arr['phone']);
        $counterparty->setEmail('');
        $result = $counterparty->save();

        if($result->errors[0] == 'Phone invalid format'){
            return [
                "errors" => "Недопустимый формат телефона"
            ];
        }
        //Получим контактных персон для контрагента получателя:
        $counterpartyRecipient = $result->data[0]->Ref;
        $contactPersonRecipient = $result->data[0]->ContactPerson->data[0]->Ref;

        //Если нет контрагента отправителя в городе , создаем там контрагента отправителя. Контрагент создастся через несколько минут:
        $data = new \NovaPoshta\MethodParameters\Counterparty_cloneLoyaltyCounterpartySender();
        $data->setCityRef($citySender);
        $result = \NovaPoshta\ApiModels\Counterparty::cloneLoyaltyCounterpartySender($data);
        $counterpartySender = $result->data[0]->Ref;
        //Получим контактных персон для контрагента отправителя:

        $data = new \NovaPoshta\MethodParameters\Counterparty_getCounterpartyContactPersons();
        $data->setRef($counterpartySender);
        $result = \NovaPoshta\ApiModels\Counterparty::getCounterpartyContactPersons($data);
        $contactPersonSender = $result->data[0]->Ref;
        //Для контрагента отправителя получим склад отправки:
/*
        $data = new \NovaPoshta\MethodParameters\Address_getWarehouses();
        $data->setCityRef($citySender);
        $result = \NovaPoshta\ApiModels\Address::getWarehouses($data);
       */
        $addressSender = "f0c6f374-427e-11e6-a9f2-005056887b8d";
        //Создадим адрес для получателя:
        if(preg_match("/ідділення/",$arr['secession'])){
            //echo 'отделение';
            //$data->setCityRef($cityRecipient);
            $connect = new Database(HOST, DB, USER, PASS);
            $query = $connect->db->prepare('SELECT * FROM city WHERE ref="' . $cityRecipient . '"');
            $query->execute();
            $result = $query->fetchAll();
            $query = $connect->db->prepare('SELECT * FROM secession WHERE id_city="' . $result[0]['id'] . '"');
            $query->execute();
            $result = $query->fetchAll();
            //$result = \NovaPoshta\ApiModels\Address::getWarehouses($data);
            $addressRecipient = $this->warehouseSet($result, $arr);
            if ($addressRecipient === false) {
                return [
                    "errors" => "Отделение не найдено"
                ];
            }
            $bool = true;
        }else{
            $bool = false;
        }
        $data = new \NovaPoshta\MethodParameters\Address_getWarehouses();

        if($bool) {
            //Теперь получим тип услуги:

            $result = \NovaPoshta\ApiModels\Common::getServiceTypes();

            $serviceType = $result->data[2]->Ref; // Выбрали: WarehouseWarehouse
        }else{
            //Теперь получим тип услуги:

            $result = \NovaPoshta\ApiModels\Common::getServiceTypes();

            $serviceType = $result->data[3]->Ref; // Выбрали: WarehouseDoors
        }
        //Выбираем плательщика:
        if($bool) {
            $result = \NovaPoshta\ApiModels\Common::getTypesOfPayers();

            $payerType = $result->data[0]->Ref; // Выбрали: Sender
            // Оплата

            $result = \NovaPoshta\ApiModels\Common::getPaymentForms();
            $paymentMethod = $result->data[1]->Ref; // Выбрали: Cash

            //Тип груза:
            $result = \NovaPoshta\ApiModels\Common::getCargoTypes();

            $cargoType = $result->data[4]->Ref; // Выбрали: Parcel
            //Мы выбрали все данные которые нам нужны для создания ЭН. Создаем ЭН:

            // Контрагент отправитель
            $sender = new \NovaPoshta\Models\CounterpartyContact();
            $sender->setCity($citySender)
                ->setRef($counterpartySender)
                ->setAddress($addressSender)
                ->setContact($contactPersonSender)
                ->setPhone('+380682399646');

            // Контрагент получатель
            $recipient = new \NovaPoshta\Models\CounterpartyContact();
            $recipient->setCity($cityRecipient)
                ->setRef($counterpartyRecipient)
                ->setAddress($addressRecipient)
                ->setContact($contactPersonRecipient)
                ->setPhone($arr['phone']);

            // Выбираем тип
            $result = \NovaPoshta\ApiModels\Common::getTypesOfPayersForRedelivery();
            $redeliveryPayer = $result->data[0]->Ref;
            $internetDocument = new \NovaPoshta\ApiModels\InternetDocument();
            $internetDocument->setSender($sender)
                ->setRecipient($recipient)
                ->setServiceType($serviceType)
                ->setPayerType($payerType)
                ->setPaymentMethod($paymentMethod)
                ->setCargoType($cargoType)
                ->setWeight(0.5)//$arr['width']
                ->setSeatsAmount($arr['count_delivery'])
                ->setCost($arr['price'])
                ->setDescription($numberInv)
                ->setDateTime(date("d.m.Y"));
            $result = $internetDocument->save();
            $refInternetDocument = $result->data[0]->Ref;
        }else{
            $date = date("d.m.Y");
            $str = "
        {
            \"apiKey\": \"df728330f70c4af25d66d9f8b1a5a191\",
            \"modelName\": \"AddressGeneral\",
            \"calledMethod\": \"getSettlements\",
            \"methodProperties\": {
                \"FindByString\":\"{$arr['city']}\"
            }
        }
        ";
            $curl = curl_init('http://testapi.novaposhta.ua/v2.0/json/AddressGeneral/getSettlements/');
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($curl, CURLOPT_HTTPHEADER, [
                'Content-Type: application/json'
            ]);
            curl_setopt($curl, CURLOPT_POST, 1);
            curl_setopt($curl, CURLOPT_POSTFIELDS, $str);
            $result = curl_exec($curl);
            $result = json_decode($result, true);
            $huj = $result;
            if(!empty($result['errors'])){
                $connect = new Database(HOST, DB, USER, PASS);
                $query = $connect->db->prepare('SELECT * FROM city WHERE ref="' . $cityRecipient . '"');
                $query->execute();
                $result = $query->fetchAll();
                $result['data'][0]['Ref'] = $result[0]['ref'];
            }else{
                $result = $huj;
            }
            if(empty($result['data'])){
                return [
                    "errors" => "Город указан на русском"
                ];
            }
            $addr = explode(',', $arr['secession']);
            $str = "
        {
            \"apiKey\": \"df728330f70c4af25d66d9f8b1a5a191\",
            \"modelName\": \"Address\",
            \"calledMethod\": \"searchSettlementStreets\",
            \"methodProperties\": {
                \"StreetName\":\"{$addr[1]}\",
                \"SettlementRef\":\"{$result['data'][2]['Ref']}\",
                \"Limit\": 5
            }
        }
        ";
            $curl = curl_init('http://testapi.novaposhta.ua/v2.0/json/Address/searchSettlementStreets/');
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($curl, CURLOPT_HTTPHEADER, [
                'Content-Type: application/json'
            ]);
            curl_setopt($curl, CURLOPT_POST, 1);
            //curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($group));
            curl_setopt($curl, CURLOPT_POSTFIELDS, $str);
            $result = curl_exec($curl);
            $result = json_decode($result, true);
            curl_close($curl);
            $addressSearch = $result['data'][0]['Addresses'];
            foreach ($addressSearch as $key => $value){
                if(preg_match("/{$addr[0]}/",$value['StreetsTypeDescription'])){//StreetsTypeDescription
                    $addr[1] = $value['Present'];
                    break;
                }
            }
            if(empty($result['data'])){
                return [
                    "errors" => "Улица не найдена"
                ];
            }

            $addr[0] = trim($addr[0]);
            $addr[2] = trim($addr[2]);
            $addr[3] = trim($addr[3]);
            $api = ANP;
            $str = "
        {
            \"apiKey\": \"{$api}\",
            \"modelName\": \"InternetDocument\",
            \"calledMethod\": \"save\",
            \"methodProperties\": {
                \"NewAddress\": \"1\",
                \"PayerType\": \"Sender\",
                \"PaymentMethod\": \"Cash\",
                \"CargoType\": \"Parcel\",
                \"Weight\": \"{$arr['width']}\",
                \"ServiceType\": \"WarehouseDoors\",
                \"SeatsAmount\": \"{$arr['count_delivery']}\",
                \"Description\": \"139express\",
                \"Cost\": \"200\",
                \"CitySender\": \"{$citySender}\",
                \"Sender\": \"{$counterpartySender}\",
                \"SenderAddress\": \"{$addressSender}\",
                \"ContactSender\": \"{$contactPersonSender}\",
                \"SendersPhone\": \"380682399646\",
                \"RecipientCityName\": \"{$arr['city']}\",
                \"RecipientAddressName\": \"{$addr[1]}\",
                \"RecipientHouse\": \"{$addr[2]}\",
                \"RecipientFlat\": \"{$addr[3]}\",
                \"RecipientName\": \"{$arr['recipient_lastname']} {$arr['recipient_firstname']}\",
                \"RecipientType\": \"PrivatePerson\",
                \"RecipientsPhone\": \"{$arr['phone']}\",
                \"DateTime\": \"{$date}\"
            }
        }
        ";
            $curl = curl_init('http://testapi.novaposhta.ua/v2.0/json/Address/searchSettlementStreets/');
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($curl, CURLOPT_HTTPHEADER, [
                'Content-Type: application/json'
            ]);
            curl_setopt($curl, CURLOPT_POST, 1);
            //curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($group));
            curl_setopt($curl, CURLOPT_POSTFIELDS, $str);
            $result = curl_exec($curl);
            $result = json_decode($result, true);
        }
        if(!empty($result->errors)){
            if($result->errors[0] == 'Recipient not selected'){
                return [
                    'errors' => 'Не верно указаны имя или фамилия получателя'
                ];
            }else{
                return [
                    'errors' => "Данные получателя указаны не верно"
                ];
            }
        }
        //Получить ссылку на печать ЭН:
        if (!empty($result->errors)) {
            $return['errors'] = $result->errors;
            $fd = fopen("console_log.txt", 'a+');
            date_default_timezone_set('Europe/Kiev');
            $str = "---------------------{" . date(date("Y-m-d H:i:s")) . "}-----------------------\n";
            for ($i = 0; $i < count($result->errors); $i++) {
                $str .= $i + 1 . " . {$result->errors[$i]}\n";
            }
            $str .= "----------{data}-------------\n";
            foreach ($arr as $key => $value) {
                $str .= $key . " = {$value}\n";
            }
            $str .= "-----------------------------------------------------------------\n";
            $str .= "#################################################################\n";
            fwrite($fd, $str);
            fclose($fd);
            return $return;
        }
        $data = new \NovaPoshta\MethodParameters\InternetDocument_printDocument();
        if($bool){
            $link = "http://testapi.novaposhta.ua/orders/printMarkings/orders[]/{$result->data[0]->Ref}/type/pdf/apiKey/". ANP;
            $this->updateInfoDelivery($result,$link,$arr);
        }else{
            $link = "http://testapi.novaposhta.ua/orders/printMarkings/orders[]/{$result['data'][0]['Ref']}/type/pdf/apiKey/" . ANP;
            $this->updateInfoDelivery($result,$link,$arr,false);
        }

        $return['internetDocument'] = $result;
        $return['link'] = $link;

        return $return;
    }


}