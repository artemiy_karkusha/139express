<?php

/**
 * Created by PhpStorm.
 * User: Artemiy Karkusha
 * Date: 6/1/2018
 * Time: 12:52 PM
 */

use NovaPoshta\Config;

class model_novaposhta_issue extends Model
{

    public $counterForwarding = 0;

    public function __construct()
    {
        $this->connect = new Database(HOST, DB, USER, PASS);
        Config::setApiKey(ANP);
        Config::setFormat(Config::FORMAT_JSONRPC2);
        Config::setLanguage(Config::LANGUAGE_UA);
    }

    public function getTable()
    {
        $str = "";
        $connect = new Database(HOST, DB, USER, PASS);
        $query = $connect->db->prepare('SELECT * FROM group_dispatch WHERE form_save = 1 and send_sms = 1 and error IS NOT NULL');
        $query->execute();
        $arr = $query->fetchAll();
        if (empty($arr)) {
            return false;
        }
        foreach ($arr as $key => $value) {
            $query = $connect->db->prepare('SELECT * FROM dispatch WHERE id_group=' . $value['id']);
            $query->execute();
            $data = $query->fetchAll();
            $number_delivery = $data[0]['inv_num_china'];
            $row = $key + 1;
            $str .= "<tr>
                            <th scope=\"row\">$row</th>
                            <td>$number_delivery</td>
                            <td>{$value['error']}</td>
                            <td>
                                <form action='/edit' method='post'>
                                <button class=\"btn btn-primary\" name='search' value=\"{$number_delivery}\">Изменить</button>
                                </form>
                                <form action='/novaposhta_issue' method='post'>
                                <input type='text' hidden='hidden' name='id' value=\"{$value['id']}\">
                                <button style='margin-top: 8px;' class=\"btn btn-primary\" name='issue'>Оформить</button><br>
                                <button style='margin-top: 8px;' class=\"btn btn-primary\" name='delete'>Удалить</button>
                                </form>
                            </td>
                        </tr>
                ";
        }
        return $str;
    }

    private function addIndex($index)
    {
        if (strlen($index) <= 4) {
            return '0' . $index;
        } else {
            return $index;
        }
    }

    public function setScan($id)
    {
        try {
            $connect = new Database(HOST, DB, USER, PASS);
            $data = [
                'scan' => 1,
                'inv_num_china' => $id,
            ];
            $sql = "UPDATE dispatch SET scan = :scan
                    WHERE inv_num_china = :inv_num_china";
            $statement = $connect->db->prepare($sql);
            $statement->execute($data);
            return TRUE;
        } catch (Exception $e) {
            return FALSE;
        }
    }


    public function leftNum($arr)
    {
        $body = '';
        foreach ($arr as $key => $value) {
            $body .= '<h3>' . $value . '</h3>';
        }
        return $body;
    }


    public function getData()
    {
        $connect = new Database(HOST, DB, USER, PASS);
        $query = $connect->db->prepare('SELECT * FROM group_dispatch WHERE form_save = 1 and send_sms = 1 and error IS NULL and inv_num_np IS NULL');
        $query->execute();
        $data = $query->fetchAll();
        return $data;
    }

    public function getDataForId($id)
    {
        $connect = new Database(HOST, DB, USER, PASS);
        $query = $connect->db->prepare('SELECT * FROM group_dispatch WHERE id =' . $id);
        $query->execute();
        $data = $query->fetchAll();
        return $data[0];
    }
    private function warehouseSet($object, $arr)
    {
        $data = $object;
        if (strripos($arr['secession'], ":")) {
            $arr = explode(":", $arr['secession']);
            if (strripos($arr[0], "(")) {
                $arr = explode("(", $arr[0]);
                $arr = trim($arr[0]);
            } else {
                $arr = (string)trim($arr[0]);
            }
            for ($i = 0; $i < count($data); $i++) {
                if (count($data) == 1) {
                    return $data[$i]['ref'];
                }
                if (preg_match("/$arr/", $data[$i]['description'])) {
                    return $data[$i]['ref'];
                }
            }
        } else {
            $arr = $arr['secession'];
            for ($i = 0; $i < count($data); $i++) {
                if (count($data) == 1) {
                    return $data[$i]['ref'];
                }
                if (preg_match("/$arr/", $data[$i]['description'])) {
                    return $data[$i]['ref'];
                }
            }
        }

        return false;
    }

    protected function updateInfoDelivery($internetDocument, $link, $arr, $bool = true)
    {
        //CostOnSite стоимость доставки
        //Update link,cost shipping, barcode
        try {
            $connect = new Database(HOST, DB, USER, PASS);
            if ($bool) {
                $data = [
                    'inv_num_np' => $internetDocument->data[0]->IntDocNumber,
                    'shipping_cost' => $internetDocument->data[0]->CostOnSite,
                    'ref' => $internetDocument->data[0]->Ref,
                    'delivery_time' => $internetDocument->data[0]->EstimatedDeliveryDate,
                    'link_print' => $link,
                    'scan' => 1,
                    'id' => $arr['id'],
                    'error' => null
                ];
            } else {
                $data = [
                    'inv_num_np' => $internetDocument['data'][0]['IntDocNumber'],
                    'shipping_cost' => $internetDocument['data'][0]['CostOnSite'],
                    'ref' => $internetDocument['data'][0]['Ref'],
                    'delivery_time' => $internetDocument['data'][0]['EstimatedDeliveryDate'],
                    'link_print' => $link,
                    'scan' => 1,
                    'id' => $arr['id'],
                    'error' => null
                ];
            }
            $sql = "UPDATE group_dispatch SET inv_num_np = :inv_num_np,
                                shipping_cost = :shipping_cost,
                                ref = :ref,
                                delivery_time = :delivery_time,
                                scan = :scan,
                                link_print = :link_print,
                                error = :error
                    WHERE id = :id";
            $statement = $connect->db->prepare($sql);
            $statement->execute($data);
            return TRUE;
        } catch (Exception $e) {
            return FALSE;
        }
    }

    public function updateError($id, $error)
    {
        try {
            $connect = new Database(HOST, DB, USER, PASS);
            $data = [
                'error' => $error,
                'id' => $id,
            ];
            $sql = 'UPDATE group_dispatch SET error = :error
                    WHERE id = :id';
            $statement = $connect->db->prepare($sql);
            $statement->execute($data);
            return TRUE;
        } catch (Exception $e) {
            echo $e->getMessage();
            return FALSE;
        }
    }

    public function addForwarding($arr)
    {
        //Выбираем город отправителя:
        $data = new \NovaPoshta\MethodParameters\Address_getCities();
        $data->setFindByString('Киев');
        $result = \NovaPoshta\ApiModels\Address::getCities($data);
        $citySender = $result->data[0]->Ref;

        //Выбираем город получателя:
        $data = new \NovaPoshta\MethodParameters\Address_getCities();
        $data->setFindByString($arr['city']);
        $result = \NovaPoshta\ApiModels\Address::getCities($data);

        $cityRecipient = $result->data[0]->Ref;
        if (empty($cityRecipient)) {
            return [
                "errors" => "Город не найден"
            ];
        }
        //Если нет контрагента отправителя в городе Киев, создаем там контрагента отправителя. Контрагент создастся через несколько минут:
        //Выбираем тип контрагента:
        $result = \NovaPoshta\ApiModels\Common::getTypesOfCounterparties();
        $counterpartyType = $result->data[1]->Ref;// Приватная


        //Создаем контрагента получателя:
        $counterparty = new \NovaPoshta\ApiModels\Counterparty();
        $counterparty->setCounterpartyProperty(\NovaPoshta\ApiModels\Counterparty::RECIPIENT);
        $counterparty->setCityRef($cityRecipient);
        $counterparty->setCounterpartyType($counterpartyType);
        $counterparty->setFirstName($arr['recipient_lastname']);
        $counterparty->setLastName($arr['recipient_firstname']);
        $counterparty->setPhone($arr['phone']);
        $counterparty->setEmail('');
        $result = $counterparty->save();
        if ($result->errors[0] == 'Phone invalid format') {
            return [
                "errors" => "Недопустимый формат телефона"
            ];
        }
        //Получим контактных персон для контрагента получателя:
        $counterpartyRecipient = $result->data[0]->Ref;
        $contactPersonRecipient = $result->data[0]->ContactPerson->data[0]->Ref;

        //Если нет контрагента отправителя в городе , создаем там контрагента отправителя. Контрагент создастся через несколько минут:
        $data = new \NovaPoshta\MethodParameters\Counterparty_cloneLoyaltyCounterpartySender();
        $data->setCityRef($citySender);
        $result = \NovaPoshta\ApiModels\Counterparty::cloneLoyaltyCounterpartySender($data);
        $counterpartySender = $result->data[0]->Ref;
        //Получим контактных персон для контрагента отправителя:

        $data = new \NovaPoshta\MethodParameters\Counterparty_getCounterpartyContactPersons();
        $data->setRef($counterpartySender);
        $result = \NovaPoshta\ApiModels\Counterparty::getCounterpartyContactPersons($data);
        $contactPersonSender = $result->data[0]->Ref;
        //Для контрагента отправителя получим склад отправки:

        $data = new \NovaPoshta\MethodParameters\Address_getWarehouses();
        $data->setCityRef($citySender);
        $result = \NovaPoshta\ApiModels\Address::getWarehouses($data);
        $addressSender = "f0c6f374-427e-11e6-a9f2-005056887b8d";
        //Создадим адрес для получателя:
        if (preg_match("/ідділення/", $arr['secession'])) {
            //echo 'отделение';
            //$data->setCityRef($cityRecipient);
            $connect = new Database(HOST, DB, USER, PASS);
            $query = $connect->db->prepare('SELECT * FROM city WHERE ref="' . $cityRecipient . '"');
            $query->execute();
            $result = $query->fetchAll();
            $query = $connect->db->prepare('SELECT * FROM secession WHERE id_city="' . $result[0]['id'] . '"');
            $query->execute();
            $result = $query->fetchAll();
            //$result = \NovaPoshta\ApiModels\Address::getWarehouses($data);
            $addressRecipient = $this->warehouseSet($result, $arr);
            if ($addressRecipient === false) {
                return [
                    "errors" => "Отделение не найдено"
                ];
            }
            $bool = true;
        } else {
            $bool = false;
        }
        $data = new \NovaPoshta\MethodParameters\Address_getWarehouses();

        if ($bool) {
            //Теперь получим тип услуги:

            $result = \NovaPoshta\ApiModels\Common::getServiceTypes();

            $serviceType = $result->data[2]->Ref; // Выбрали: WarehouseWarehouse
        } else {
            //Теперь получим тип услуги:

            $result = \NovaPoshta\ApiModels\Common::getServiceTypes();

            $serviceType = $result->data[3]->Ref; // Выбрали: WarehouseDoors
        }
        //Выбираем плательщика:
        if ($bool) {
            $result = \NovaPoshta\ApiModels\Common::getTypesOfPayers();

            $payerType = $result->data[0]->Ref; // Выбрали: Sender
            // Оплата

            $result = \NovaPoshta\ApiModels\Common::getPaymentForms();
            $paymentMethod = $result->data[1]->Ref; // Выбрали: Cash

            //Тип груза:
            $result = \NovaPoshta\ApiModels\Common::getCargoTypes();

            $cargoType = $result->data[4]->Ref; // Выбрали: Parcel
            //Мы выбрали все данные которые нам нужны для создания ЭН. Создаем ЭН:

            // Контрагент отправитель
            $sender = new \NovaPoshta\Models\CounterpartyContact();
            $sender->setCity($citySender)
                ->setRef($counterpartySender)
                ->setAddress($addressSender)
                ->setContact($contactPersonSender)
                ->setPhone('+380682399646');

            // Контрагент получатель
            $recipient = new \NovaPoshta\Models\CounterpartyContact();
            $recipient->setCity($cityRecipient)
                ->setRef($counterpartyRecipient)
                ->setAddress($addressRecipient)
                ->setContact($contactPersonRecipient)
                ->setPhone($arr['phone']);

            // Выбираем тип
            $result = \NovaPoshta\ApiModels\Common::getTypesOfPayersForRedelivery();
            $redeliveryPayer = $result->data[0]->Ref;
            $internetDocument = new \NovaPoshta\ApiModels\InternetDocument();
            $internetDocument->setSender($sender)
                ->setRecipient($recipient)
                ->setServiceType($serviceType)
                ->setPayerType($payerType)
                ->setPaymentMethod($paymentMethod)
                ->setCargoType($cargoType)
                ->setWeight(0.5)//$arr['width']
                ->setSeatsAmount($arr['count_delivery'])
                ->setCost($arr['price'])
                ->setDescription("139express")
                ->setDateTime(date("d.m.Y"));
            $result = $internetDocument->save();
            $refInternetDocument = $result->data[0]->Ref;
        } else {
            $date = date("d.m.Y");
            $str = "
        {
            \"apiKey\": \"df728330f70c4af25d66d9f8b1a5a191\",
            \"modelName\": \"AddressGeneral\",
            \"calledMethod\": \"getSettlements\",
            \"methodProperties\": {
                \"FindByString\":\"{$arr['city']}\"
            }
        }
        ";
            $curl = curl_init('http://testapi.novaposhta.ua/v2.0/json/AddressGeneral/getSettlements/');
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($curl, CURLOPT_HTTPHEADER, [
                'Content-Type: application/json'
            ]);
            curl_setopt($curl, CURLOPT_POST, 1);
            curl_setopt($curl, CURLOPT_POSTFIELDS, $str);
            $result = curl_exec($curl);
            $result = json_decode($result, true);
            $huj = $result;
            if (!empty($result['errors'])) {
                $connect = new Database(HOST, DB, USER, PASS);
                $query = $connect->db->prepare('SELECT * FROM city WHERE ref="' . $cityRecipient . '"');
                $query->execute();
                $result = $query->fetchAll();
                $result['data'][0]['Ref'] = $result[0]['ref'];
            } else {
                $result = $huj;
            }
            if (empty($result['data'])) {
                return [
                    "errors" => "Город указан на русском"
                ];
            }
            $addr = explode(',', $arr['secession']);
            $str = "
        {
            \"apiKey\": \"df728330f70c4af25d66d9f8b1a5a191\",
            \"modelName\": \"Address\",
            \"calledMethod\": \"searchSettlementStreets\",
            \"methodProperties\": {
                \"StreetName\":\"{$addr[1]}\",
                \"SettlementRef\":\"{$result['data'][2]['Ref']}\",
                \"Limit\": 5
            }
        }
        ";
            $curl = curl_init('http://testapi.novaposhta.ua/v2.0/json/Address/searchSettlementStreets/');
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($curl, CURLOPT_HTTPHEADER, [
                'Content-Type: application/json'
            ]);
            curl_setopt($curl, CURLOPT_POST, 1);
            //curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($group));
            curl_setopt($curl, CURLOPT_POSTFIELDS, $str);
            $result = curl_exec($curl);
            $result = json_decode($result, true);
            curl_close($curl);
            $addressSearch = $result['data'][0]['Addresses'];
            foreach ($addressSearch as $key => $value) {
                if (preg_match("/{$addr[0]}/", $value['StreetsTypeDescription'])) {//StreetsTypeDescription
                    $addr[1] = $value['Present'];
                    break;
                }
            }
            if (empty($result['data'])) {
                return [
                    "errors" => "Улица не найдена"
                ];
            }

            $addr[0] = trim($addr[0]);
            $addr[2] = trim($addr[2]);
            $addr[3] = trim($addr[3]);
            $api = ANP;
            $str = "
        {
            \"apiKey\": \"{$api}\",
            \"modelName\": \"InternetDocument\",
            \"calledMethod\": \"save\",
            \"methodProperties\": {
                \"NewAddress\": \"1\",
                \"PayerType\": \"Sender\",
                \"PaymentMethod\": \"Cash\",
                \"CargoType\": \"Parcel\",
                \"Weight\": \"{$arr['width']}\",
                \"ServiceType\": \"WarehouseDoors\",
                \"SeatsAmount\": \"{$arr['count_delivery']}\",
                \"Description\": \"139express\",
                \"Cost\": \"200\",
                \"CitySender\": \"{$citySender}\",
                \"Sender\": \"{$counterpartySender}\",
                \"SenderAddress\": \"{$addressSender}\",
                \"ContactSender\": \"{$contactPersonSender}\",
                \"SendersPhone\": \"380682399646\",
                \"RecipientCityName\": \"{$arr['city']}\",
                \"RecipientAddressName\": \"{$addr[1]}\",
                \"RecipientHouse\": \"{$addr[2]}\",
                \"RecipientFlat\": \"{$addr[3]}\",
                \"RecipientName\": \"{$arr['recipient_lastname']} {$arr['recipient_firstname']}\",
                \"RecipientType\": \"PrivatePerson\",
                \"RecipientsPhone\": \"{$arr['phone']}\",
                \"DateTime\": \"{$date}\"
            }
        }
        ";
            $curl = curl_init('http://testapi.novaposhta.ua/v2.0/json/Address/searchSettlementStreets/');
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($curl, CURLOPT_HTTPHEADER, [
                'Content-Type: application/json'
            ]);
            curl_setopt($curl, CURLOPT_POST, 1);
            //curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($group));
            curl_setopt($curl, CURLOPT_POSTFIELDS, $str);
            $result = curl_exec($curl);
            $result = json_decode($result, true);
        }
        if (!empty($result->errors)) {
            if ($result->errors[0] == 'Recipient not selected') {
                return [
                    'errors' => 'Не верно указаны имя или фамилия получателя'
                ];
            } else {
                return [
                    'errors' => "Данные получателя указаны не верно"
                ];
            }
        }
        //Получить ссылку на печать ЭН:
        if (!empty($result->errors)) {
            $return['errors'] = $result->errors;
            $fd = fopen("console_log.txt", 'a+');
            date_default_timezone_set('Europe/Kiev');
            $str = "---------------------{" . date(date("Y-m-d H:i:s")) . "}-----------------------\n";
            for ($i = 0; $i < count($result->errors); $i++) {
                $str .= $i + 1 . " . {$result->errors[$i]}\n";
            }
            $str .= "----------{data}-------------\n";
            foreach ($arr as $key => $value) {
                $str .= $key . " = {$value}\n";
            }
            $str .= "-----------------------------------------------------------------\n";
            $str .= "#################################################################\n";
            fwrite($fd, $str);
            fclose($fd);
            return $return;
        }
        $data = new \NovaPoshta\MethodParameters\InternetDocument_printDocument();
        if ($bool) {
            $link = "http://testapi.novaposhta.ua/orders/printMarkings/orders[]/{$result->data[0]->Ref}/type/pdf/apiKey/" . ANP;
            $this->updateInfoDelivery($result, $link, $arr);
        } else {
            $link = "http://testapi.novaposhta.ua/orders/printMarkings/orders[]/{$result['data'][0]['Ref']}/type/pdf/apiKey/" . ANP;
            $this->updateInfoDelivery($result, $link, $arr, false);
        }

        $return['internetDocument'] = $result;
        $return['link'] = $link;

        return $return;
    }


    protected function getDataExcel(){
        $connect = new Database(HOST, DB, USER, PASS);
        $query = $connect->db->prepare('SELECT * FROM group_dispatch WHERE form_save = 1 and send_sms = 1 and error IS NOT NULL');
        $query->execute();
        $data = $query->fetchAll();
        $str = '';
        for ($i = 0; $i < count($data); $i++) {
            $query = $connect->db->prepare('SELECT inv_num_china FROM dispatch WHERE id_group = ' . $data[$i]['id']);
            $query->execute();
            $countInv = $query->fetchAll();
            for ($j = 0; $j < count($countInv); $j++) {
                $str .= $countInv[$j]['inv_num_china'] . ";";
                //$countInv[$j] = $countInv[$j]['inv_num_china'];
            }
            $data[$i]['inv_china'] = $str;
            unset($str);
        }
        return $data;
    }

    public function uploadErrorsDelivery(){
        $data = $this->getDataExcel();
        if(empty($data)){
            return [
                'errors' => 'Все доставки оформлено правильно'
            ];
        }
        //Класс для чтения
        try {
            require './application/core/Classes/PHPExcel.php';
            //Класс для работы с датами в Excel
            require './application/core/Classes/PHPExcel/Writer/Excel5.php';
        } catch (Exception $e) {
            echo $e->getMessage();
        }
        $border = [
            'borders' => [
                'bottom' => [
                    'style' => PHPExcel_Style_Border::BORDER_MEDIUM,
                    'color' => [
                        'rgb' => '808080'
                    ]
                ],
                'top' => [
                    'style' => PHPExcel_Style_Border::BORDER_MEDIUM,
                    'color' => [
                        'rgb' => '808080'
                    ]
                ],
                'left' => [
                    'style' => PHPExcel_Style_Border::BORDER_MEDIUM,
                    'color' => [
                        'rgb' => '808080'
                    ]
                ],
                'right' => [
                    'style' => PHPExcel_Style_Border::BORDER_MEDIUM,
                    'color' => [
                        'rgb' => '808080'
                    ]
                ]
            ]
        ];
        //Массив с настройками для выравнивания текста в ячейках
        $alignmentCenter = [
            'alignment' => [
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER
            ]
        ];
        $alignmentCenterVertical = [
            'alignment' => [
                'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER
            ]
        ];

        $colorRed = [
            'fill' => [
                'type' => 'solid',
                'color' => [
                    'rgb' => 'FF0000'
                ]
            ]
        ];
        $colorGreen = [
            'fill' => [
                'type' => 'solid',
                'color' => [
                    'rgb' => '00CD0E'
                ]
            ]
        ];
        $colorGrey = [
            'fill' => [
                'type' => 'solid',
                'color' => [
                    'rgb' => '8E91EF'
                ]
            ]
        ];
        $colorCommon = [
            'fill' => [
                'type' => 'solid',
                'color' => [
                    'rgb' => 'E4E4E4'
                ]
            ]
        ];
        $fontTop = [
            'font' => [
                'name' => 'Arial',
                'size' => 10,
                'bold' => true
            ]
        ];
        // Создаем объект класса PHPExcel
        $xls = new PHPExcel();
        // Устанавливаем индекс активного листа
        $xls->setActiveSheetIndex(0);
        // Получаем активный лист
        $sheet = $xls->getActiveSheet();
        // Подписываем лист
        $sheet->setTitle('Report');
        // Устанавливаем название и оформление столбцов
        $sheet->setCellValue("A1", 'Number China');
        $sheet->getColumnDimension('A')->setWidth(30);
        // Выравнивание текста
        $sheet->getStyle('A1')->applyFromArray($alignmentCenter);
        $sheet->setCellValue("B1", 'Телефон');
        $sheet->getColumnDimension('B')->setWidth(30);
        $sheet->setCellValue("C1", 'ФИО');
        $sheet->getColumnDimension('С')->setWidth(70);
        // Выравнивание текста
        $sheet->getStyle('C1')->applyFromArray($alignmentCenter);
        $sheet->setCellValue("D1", 'Город');
        $sheet->getStyle('D1')->applyFromArray($alignmentCenter);
        $sheet->getColumnDimension('D')->setWidth(30);
        $sheet->setCellValue("E1", 'Адрес');
        $sheet->getStyle('E1')->applyFromArray($alignmentCenter);
        $sheet->getColumnDimension('E')->setWidth(60);
        $sheet->setCellValue("F1", 'Ошибка');
        $sheet->getStyle('F1')->applyFromArray($alignmentCenter);
        $sheet->getColumnDimension('F')->setWidth(40);
        for ($i = 0; $i < count($data); $i++) {
            $j = $i + 2;
            $sheet->setCellValue("A" . $j, $data[$i]['inv_china']);
            $sheet->getStyle('A' . $j)->applyFromArray($alignmentCenter);
            $sheet->setCellValue("B" . $j, $data[$i]['phone']);
            $sheet->getStyle('B' . $j)->applyFromArray($alignmentCenter);
            $sheet->setCellValue("C" . $j, $data[$i]['recipient_firstname'] . " " . $data[$i]['recipient_lastname']);
            $sheet->getStyle('C' . $j)->applyFromArray($alignmentCenter);
            $sheet->setCellValue("D" . $j, $data[$i]['city']);
            $sheet->getStyle('D' . $j)->applyFromArray($alignmentCenter);
            $sheet->setCellValue("E" . $j, $data[$i]['secession']);
            $sheet->getStyle('F' . $j)->applyFromArray($alignmentCenter);
            $sheet->setCellValue("F" . $j, $data[$i]['error']);
            $sheet->getStyle('F' . $j)->applyFromArray($alignmentCenter);
        }
        //Рамки
        for ($i = 0; $i < 6; $i++) {
            for ($j = 0; $j < count($data) +2; $j++) {
                $sheet->getStyleByColumnAndRow($i, $j)->applyFromArray($border);
            }
        }
        date_default_timezone_set('Europe/Kiev');
        $date_time = date("Y-m-d H:i:s");
        $objWriter = new PHPExcel_Writer_Excel5($xls);
        header('Content-type: application/vnd.ms-excel;charset=utf-8');
        header('Content-Disposition: attachment; filename="errors-' . $date_time . '.xls"');
        $objWriter->save('php://output');
        exit;
    }

    public function deleteDelivery($id){
        try {
            $connect = new Database(HOST, DB, USER, PASS);
            $query = $connect->db->prepare('SELECT * FROM dispatch WHERE id_group =' . $id);
            $query->execute();
            $arr = $query->fetchAll();
            foreach ($arr as $key => $value) {
                $query = $connect->db->prepare('DELETE FROM dispatch WHERE id =  :id');
                $query->bindParam(":id", $value['id']);
                $query->execute();
            }
            $query = $connect->db->prepare('DELETE FROM group_dispatch WHERE id =  :id');
            $query->bindParam(":id", $id);
            $query->execute();
        }catch (Exception $e){
            $e->getMessage();
            return false;
        }
    }

}