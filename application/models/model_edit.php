<?php
/**
 * Created by PhpStorm.
 * User: artem
 * Date: 8/3/2018
 * Time: 1:25 PM
 */

class model_edit extends  Model{

    public function getData($id){
        $connect = new Database(HOST, DB, USER, PASS);
        try {
            $query = $connect->db->prepare('SELECT id_group FROM dispatch WHERE inv_num_china =' . $id);
            $query->execute();
            $data = $query->fetchAll();
        }catch(Exception $e){
            //$e->getMessage();
        }
        if(empty($data)){
            return [
               "errors" => "Номер не найден"
            ];

        }
        $id_group = $data[0]['id_group'];
        $query = $connect->db->prepare('SELECT * FROM group_dispatch WHERE id =' . $id_group);
        $query->execute();
        $data = $query->fetchAll()[0];
        $query = $connect->db->prepare('SELECT inv_num_china,scan FROM dispatch WHERE id_group =' . $data['id']);
        $query->execute();
        $countInv = $query->fetchAll();
        $data['inv_track'] = $countInv;
        for ($i = 0; $i < count($countInv); $i++) {
            $countInv[$i] = $countInv[$i]['inv_num_china'];
        }
        $data['inv_now'] = $id;
        $data['inv_china'] = $countInv;
        $countInv = $data['inv_track'];
        $del[] = null;
        for ($i = 0; $i < count($countInv); $i++) {
            if ($countInv[$i]['scan'] === null) {
                $countInv[$i] = $countInv[$i]['inv_num_china'];
            } else {
                $del[$i] = $i;
            }
        }
        foreach ($del as $key){
            unset($countInv[$key]);
        }
        $data['inv_track'] = $countInv;
        return $data;
    }

    public function updateDispatch($array){
        try {
            $connect = new Database(HOST, DB, USER, PASS);
            $data = [
                'recipient_firstname' => $array['first_name'],
                'recipient_lastname' => $array['surname'],
                'phone' => $array['phone'],
                'city' => $array['city'],
                'secession' => $array['secession'],
                'count_delivery' => $array['count_del'],
                'width' => $array['weight'],
                'price' => $array['cost'],
                'id' => $array['id_del'],
            ];
            $sql = "UPDATE group_dispatch SET recipient_firstname = :recipient_firstname,
                                recipient_lastname = :recipient_lastname,
                                phone = :phone,
                                city = :city,
                                secession = :secession,
                                count_delivery = :count_delivery,
                                width = :width,
                                count_delivery = :count_delivery
                    WHERE id = :id";
            $statement = $connect->db->prepare($sql);
            $statement->execute($data);
            return TRUE;
        }catch (Exception $e){
            return FALSE;
        }
    }

}