<?php

class Controller_engineering_works extends Controller
{

    function __construct()
    {
        $this->model = new model_engineering_works();
        $this->view = new View();
    }

    function action_index()
    {
        $this->auth();
        $data = $this->model->allInfoUser();
        $this->view->generate('admin/eng_work_view.php', 'admin/template_view.php',$data);
    }

}
