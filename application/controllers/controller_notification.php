<?php
/**
 * Created by PhpStorm.
 * User: artem
 * Date: 8/14/2018
 * Time: 6:10 PM
 */

class controller_notification extends Controller{

    function __construct()
    {
        $this->model = new model_notification();
        $this->view = new View();
    }

    function action_index()
    {
        $this->auth();
        $data = $this->model->allInfoUser();
        $resTable = $this->model->getTableNotSaveForm();
        if($resTable == false){
            $data['table'] = "<tr><th scope=\"row\">Не сохранённых уведомлений нет</th></tr>";

        }else{
            $data['table'] = $resTable;
        }
        if($_POST){
           if(isset($_POST['sms_send'])){
               $resultSendSms = $this->model->send();
               if(isset($resultSendSms['error'])){
                   $data['sms_send'] = $resultSendSms['error'];
               }else{
                   $data['sms_send'] = "Уведомления успешно отправлены";
               }
           }
        }
        $this->view->generate('admin/notification_view.php', 'admin/template_view.php',$data);
    }

}