<?php
/**
 * Created by PhpStorm.
 * User: artem
 * Date: 8/14/2018
 * Time: 6:14 PM
 */

class controller_delivery extends Controller
{

    function __construct()
    {
        $this->model = new model_delivery();
        $this->view = new View();
    }

    function action_index()
    {
        $data = [];
        if ($_POST){
            if(isset($_POST['submit'])){
                if(!empty($_POST['secession'])){
                    $data['resUpdateGroup'] = $this->model->updateGroup($_POST);
                }
                if(!empty($_POST['address'])){
                    $data['resUpdateGroup'] = $this->model->updateGroup($_POST, true);
                }
            }
        }
        if (isset($_GET['d'])) {
            $arr = $this->model->getData($_GET['d']);;
            if (!isset($arr['errors'])) {
                if($arr['form_save'] == true){
                    $formNotSave = $this->model->genForm($arr);
                    $data['delivery_not'] = [
                        "form" => $formNotSave
                    ];
                }else{
                    $form  = $this->model->getForm($arr);
                    $data['delivery'] = [
                        "id" => $arr['id'],
                        "firstname" => $arr['recipient_firstname'],
                        "surname" => $arr['recipient_lastname'],
                        "number" => $arr['num_delivery'],
                        "form" => $form
                    ];
                }
            }

        }
        $this->view->generate('delivery_view.php', 'delivery_template_view.php',$data);
    }

    function action_secession(){
        if($_POST){
            if(isset($_POST['city'])){
                $data = $this->model->getCity($_POST['city']);
                if($data != false){
                    echo $this->model->genOptionCity($data);
                }
            }
            if(isset($_POST['secession'])){
                $data = $this->model->getCity($_POST['secession']);
                echo $this->model->genOptionSec($data[0]);
            }
            if(isset($_POST['seces'])){
                $data = $this->model->getCity($_POST['citys']);
                echo $this->model->genOptionRtc($data[0],$_POST['seces']);
            }
            if(isset($_POST['address'])){
                $data = $this->model->getCity($_POST['city_address']);
                echo $this->model->getAddress($data[0],$_POST['address']);
            }
        }
    }
}