<?php
/**
 * Created by PhpStorm.
 * User: artem
 * Date: 8/3/2018
 * Time: 1:25 PM
 */

class controller_edit extends Controller{

    function __construct()
    {
        $this->model = new model_edit();
        $this->view = new View();
    }

    function action_index()
    {
        $this->auth();
        $data = $this->model->allInfoUser();
        if($_POST or $_GET){
            if(isset($_POST['search']) or isset($_GET['search'])){
                if($_POST){
                    $data['data'] = $this->model->getData($_POST['search']);
                }
                if($_GET){
                    $data['data'] = $this->model->getData($_GET['search']);
                }
                //debug($data);
                if(isset($data['data']['errors'])){
                    $data['errors'] = $data['data']['errors'];
                    unset($data['data']);
                }else{
                    $data['search'] = true;
                }
                //debug($data);
                //exit;
            }
            if(isset($_POST['edit_del'])){
                $data['update'] = $this->model->updateDispatch($_POST);
                $data['data'] = $this->model->getData($_POST['num_del']);
                $data['search'] = true;
            }
            if(isset($_POST['search_back'])){
                $data['search'] = false;
            }
        }
        $this->view->generate('admin/edit_view.php', 'admin/template_view.php',$data);
    }

}