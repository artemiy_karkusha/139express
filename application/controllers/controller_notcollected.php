<?php
/**
 * Created by PhpStorm.
 * User: artem
 * Date: 7/23/2018
 * Time: 2:42 PM
 */

class controller_notcollected extends Controller
{
    function __construct()
    {
        $this->model = new model_notcollected();
        $this->view = new View();
    }

    function action_index()
    {
        $this->auth();
        $data = $this->model->allInfoUser();
        //echo $refCity = $this->model->getCity('','Киев');
        //$getAllWarehouse = $this->model->getAllWarehouse($refCity);

        //debug($getAllWarehouse);
        $data['table'] = $this->model->getStatus();
        $this->view->generate('admin/notcollected_view.php', 'admin/template_view.php',$data);
    }
}