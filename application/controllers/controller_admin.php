<?php

class controller_admin extends Controller
{

    function __construct()
    {
        $this->model = new model_admin();
        $this->view = new View();
    }

    function action_index()
    {
        $this->auth();
        $data = $this->model->allInfoUser();
        //$this->model->address();
        //exit;
        if (isset($_POST['download_excel'])){
            $this->model->downloadExcel();
        }
        $this->view->generate('admin/main_view.php', 'admin/template_view.php',$data);
    }
}