<?php
/**
 * Created by PhpStorm.
 * User: artemiy
 * Date: 01.05.2018
 * Time: 13:33
 */

class controller_lost extends Controller
{

    function __construct()
    {
        $this->model = new model_lost();
        $this->view = new View();
    }

    function action_index()
    {
        $this->auth();
        $data = $this->model->allInfoUser();
        $data['table'] = $this->model->getTable();
        $this->view->generate('admin/lost_view.php', 'admin/template_view.php',$data);
    }
}