<?php
/**
 * Created by PhpStorm.
 * User: Artemiy Karkusha
 * Date: 5/23/2018
 * Time: 6:42 PM
 */
class controller_delivery_excel extends Controller
{
    function __construct()
    {
        $this->model = new model_delivery_excel();
        $this->view = new View();
    }

    function action_index()
    {
        $this->auth();
        $data = $this->model->allInfoUser();
        if (isset($_POST['upload_old'])){
            $data['addFromOldExcel'] = $this->model->addFromOldExcel();
            $data['countAddExcel'] = $this->model->countAddRow;
            $data['countDelivery'] = $groupChange = $this->model->changeTable();
        }
        if (isset($_POST['download_excel'])){
            $report = $this->model->reportExcel();
            if(isset($report['errors'])){
                $data['reportExcel'] = $report['errors'];
            }
        }
        $data['selectInfo'] = $this->model->getInfoSelect();
        $this->view->generate('admin/delivery_excel_view.php', 'admin/template_view.php',$data);
    }
}