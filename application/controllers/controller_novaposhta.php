<?php
/**
 * Created by PhpStorm.
 * User: Artemiy Karkusha
 * Date: 17.05.2018
 * Time: 21:12
 */

class controller_novaposhta extends Controller
{

    function __construct()
    {
        $this->model = new model_novaposhta();
        $this->view = new View();
    }

    function action_index()
    {
        $this->auth();
        $data = $this->model->allInfoUser();
        if ($_POST) {
            if (isset($_POST['number_delivery'])) {
                //Получаем данные с dispatch_group
                $infoDelivery = $this->model->getData($_POST['number_delivery']);
                //Если есть ошибки вывести их
                if (isset($infoDelivery['errors'])) {
                    $data['errors'] = $infoDelivery['errors'];
                }//Если поле inv_num_np не пустое открыть накладную
                elseif ($infoDelivery['inv_num_np'] === null) {
                    //Сделать апдейт поля scan = 1
                    $this->model->setScan($infoDelivery['inv_now']);
                    //Если количество мест больше 1 то вывести оставшиеся
                    if ($infoDelivery['count_delivery'] > 1) {
                        $infoDelivery = $this->model->getData($_POST['number_delivery']);
                        if (empty($infoDelivery['inv_track'])) {
                            //Если просканирована вся група то создаем накладную
                            $shipment = $this->model->addForwarding($infoDelivery);
                            if (!isset($shipment['errors'])) {
                                //Перенаправление в случае если нет ошибок
                                header("Location:{$shipment['link']}");
                                exit;
                            } else {
                                $data['errors'] = $shipment['errors'];
                            }
                        } else {
                            $data['leftNum'] = $this->model->leftNum($infoDelivery['inv_track']);
                        }
                    } else {
                        //Создать доставку на новой почте
                        $shipment = $this->model->addForwarding($infoDelivery);
                        if (!isset($shipment['errors'])) {
                            header("Location:{$shipment['link']}");
                            exit;
                        } else {
                            $data['errors'] = $shipment['errors'];
                        }
                    }
                } else {
                    header("Location:" . $infoDelivery['link_print']);
                    exit;
                }

            }
            if(isset($_POST['delivery_group'])){
                $info  = $this->model->getData($_POST['delivery_group']);
                if (isset($info['errors'])) {
                    $data['errors_group'] = $info['errors'];
                }else{
                    $data['delivery_group']['count'] = count($info['inv_china']);
                    $data['delivery_group']['number'] = $this->model->leftNum($info['inv_china']);
                }//Если поле inv_num_np не пустое открыть накладную
            }
        }
        $this->view->generate('admin/novaposhta_view.php', 'admin/template_view.php', $data);
    }
}