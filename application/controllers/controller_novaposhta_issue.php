<?php
/**
 * Created by PhpStorm.
 * User: Artemiy Karkusha
 * Date: 17.05.2018
 * Time: 21:12
 */

class controller_novaposhta_issue extends Controller
{

    function __construct()
    {
        $this->model = new model_novaposhta_issue();
        $this->view = new View();
    }

    function action_index()
    {
        $this->auth();
        $data = $this->model->allInfoUser();
        if ($_POST) {
            if (isset($_POST['delivery_issue'])) {
                $infoDelivery = $this->model->getData();
                if(!empty($infoDelivery)) {
                    foreach ($infoDelivery as $key => $value) {
                        $data['issue'] = $this->model->addForwarding($value);
                        if (isset($data['issue']['errors'])) {
                            $this->model->updateError($value['id'], $data['issue']['errors']);
                        } else {
                            $this->model->counterForwarding++;
                        }
                    }
                    $data['delivery_issue'] = $this->model->counterForwarding;
                }else{
                    $data['delivery_issue_null'] = "Доставок для оформления нету";
                }
            }
            if (isset($_POST['issue'])) {
                $infoDelivery = $this->model->getDataForId($_POST['id']);
                $data['issue'] = $this->model->addForwarding($infoDelivery);
                if (isset($data['issue']['errors'])) {
                    $this->model->updateError($infoDelivery['id'], $data['issue']['errors']);
                }
            }
            if(isset($_POST['delete'])){
                $this->model->deleteDelivery($_POST['id']);
            }
            if(isset($_POST['errorsDeliveryExcel'])){
                $report = $this->model->uploadErrorsDelivery();
                if(isset($report['errors'])){
                    $data['reportExcel'] = $report['errors'];
                }
            }

        }
        $data['table'] = $this->model->getTable();
        $this->view->generate('admin/novaposhta_issue_view.php', 'admin/template_view.php', $data);
    }
}