<!--
Created by PhpStorm.
 User: Artemiy Karkusha
 Date: 11.05.2018
 Time: 17:14
 -->

<header id="home">
<section class="hero" id="hero">
    <div id="bg">
        <img src="/img/hero-bg.jpg" alt="">
    </div>
    <div class="main_header">
        <div class="col-md-1">
        </div>
        <div class="col-md-4">
           <h1>139 Express</h1>
        </div>
        <div class="col-md-4">

        </div>
        <div  class="col-md-3 margin">
            <p href="">tel. (380)68-239-96-46</p>
        </div>
        <div class="col-md-1">
        </div>
    </div>
    <div class="container">
        <div class="row">
            <h1 class="text-slogan"> <span>Клиенты</span> для вашего бизнеса</h1><br>
                <p>Сделай больше доставок вместе с нами</p><br>
            <p class="links"><a href="/login">Войти</a></p>
    </div>
    </div>
</section>
</header>