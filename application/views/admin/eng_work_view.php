<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            На сайте технические работы
        </h1>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="error-page">
            <img src="/img/teh-work.jpg" alt="Технические работы" style="border-radius: 10px;">
            <div class="error-content">
                <h3><i class="fa fa-warning text-yellow"></i> Упс... Сайт временно не работает</h3>

                <p>
                    <h4>Это повод выпить чаю!!!</h4>
                </p>
            </div>
            <!-- /.error-content -->
        </div>
        <!-- /.error-page -->
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->

