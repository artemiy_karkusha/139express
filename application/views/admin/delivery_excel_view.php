<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Data from Excel
        </h1>
        <ol class="breadcrumb">
            <li><a href="/admin"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Delivery -> Excel</li>
        </ol>
    </section>
    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-md-6">
                <div class="box box-widget">
                    <div class="box-header with-border">
                        <h3 class="box-title">Добавить накладные</h3>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <p>Возможность добавления накладных с правильным дизайном шаблона Excel</p>
                        <p>Шаблон в формате Excel можно взять на главной странице сайта</p>
                        <form enctype="multipart/form-data" action="/delivery_excel" method="POST">
                            <b>Загрузить файл:</b> <input class="btn btn-primary icon-btn mr-10" name="upload" type="file">
                            <br>
                            <input class="btn btn-success icon-btn" name="upload_old" type="submit" value="Отправить">
                        </form>
                        <? if ($data['addFromOldExcel'] === TRUE): ?>
                            <div class="alert alert-success mt-10" role="alert" style="margin-top: 10px;">
                                Успешно загружено <?=$data['countAddExcel'];?> строк <br>
                                Создано <?=$data['countDelivery']?> доставок
                            </div>
                        <? endif; ?>
                        <? if ($data['addFromOldExcel'] === FALSE): ?>
                            <div class="alert alert-danger mt-10" role="alert" style="margin-top: 10px;">
                                Данные не загружены
                            </div>
                        <? endif; ?>
                    </div>
                    <!-- ./box-body -->
                </div>
                <!-- /.box -->
            </div>
            <!-- /.col -->
            <div class="col-md-6">
                <div class="box box-widget">
                    <div class="box-header with-border">
                        <h3 class="box-title">Загрузить отчет</h3>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <form action="/delivery_excel" method="post" target="_blank">
                            <button class="btn btn-success icon-btn" name="download_excel"><i class="fa fa-download"></i> Скачать</button>
                            <?=$data['selectInfo'];?>
                        </form>
                        <?php if(isset($data['reportExcel'])):?>
                            <div class="alert alert-danger mt-10" role="alert" style="margin-top: 10px;">
                                <?=$data['reportExcel'];?>
                            </div>
                        <?php endif;;?>
                    </div>
                    <!-- ./box-body -->
                </div>
                <!-- /.box -->
            </div>
            <!-- /.col -->
        </div>
    </div>
    </section>
</div>