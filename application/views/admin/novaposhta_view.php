<script type="text/javascript">
    function setFocus() {
        document.getElementsByTagName("input")[0].focus();
    }
    document.addEventListener("DOMContentLoaded", setFocus);
</script>
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Novaposhta
        </h1>
        <ol class="breadcrumb">
            <li><a href="/admin"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Novaposhta</li>
        </ol>
    </section>
    <!-- Main content -->
    <section class="content">
        <? if ($data['userLaw'] >= 2){ ?>
        <div class="row">
            <div class="col-md-6">
                <div class="box box-widget">
                    <div class="box-header with-border">
                        <h3 class="box-title">Введите номер накладной или подключите сканер</h3>
                    </div>
                    <div class="box-body">
                    <form method="post" target="_blank" action="/novaposhta" class="form-search form-inline"
                          onsubmit="deleteValue()">
                        <input type="text" name="number_delivery" class="number_delivery" placeholder="# invoice" required>
                        <button class="btn btn-success" type="submit">Create invoice</button>
                        <?php if (isset($data['leftNum'])): ?>
                            <div class="alert alert-info" role="alert" style="margin-top: 10px;">
                            Вы просканировали накладную котороя входит в групу<br>
                            Осталось просканировать для создания отправки:
                                <?=$data['leftNum'];?>
                            </div>
                        <?php endif;?>
                        <?php if (isset($data['errors'])): ?>
                            <div class="alert alert-danger" role="alert" style="margin-top: 10px;">
                                <?=$data['errors'];?>
                            </div>
                        <?php endif;?>
                        <hr>
                    </form>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="box box-widget">
                    <div class="box-header with-border">
                        <h3 class="box-title">Введите номер накладной для получение всех посылок группы</h3>
                    </div>
                    <div class="box-body">
                        <form method="post" action="/novaposhta" class="form-search form-inline"
                              onsubmit="deleteValue()">
                            <input type="text" name="delivery_group" class="number_delivery" placeholder="# invoice" required>
                            <button class="btn btn-success" type="submit">Посмотреть групу</button>
                            <?php if (isset($data['delivery_group'])): ?>
                                <div class="alert alert-info" role="alert" style="margin-top: 10px;">
                                    <h4>В группе <b><?=$data['delivery_group']['count'];?></b> посылок с номирами:</h4>
                                    <?=$data['delivery_group']['number'];?>
                                </div>
                            <?php endif;?>
                            <?php if (isset($data['errors_group'])): ?>
                                <div class="alert alert-danger" role="alert" style="margin-top: 10px;">
                                    <?=$data['errors_group'];?>
                                </div>
                            <?php endif;?>
                            <hr>
                        </form>
                    </div>
                </div>
            </div>

        </div>
</div>
<? } else { ?>
    <div class="page-title">
        <div>
            <h1 style="color:red">You don't have accesss this page</h1>
        </div>
        <div>
            <ul class="breadcrumb">
                <li><i class="fa fa-home fa-lg"></i></li>
                <li><a href="/admin">Main</a></li>
            </ul>
        </div>
    </div>
<? } ?>
<script>
    function deleteValue() {
        setTimeout(function () {
            var text = document.getElementsByTagName("input")[0];
            text.value = '';
        }, 1000);
    }
</script>