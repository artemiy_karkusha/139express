<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            139
            Express
        </h1>
    </section>

    <!-- Main content -->
    <section class="content">
        <!-- Info boxes -->
        <div class="row">
                <!--<div class="col-md-6">
                    <div class="box box-widget">
                        <div class="box-header with-border">
                            <h3 class="box-title">Введите номер накладной</h3>
                        </div>
                        <div class="box-body">
                            <form method="post" action="/edit" class="form-search form-inline"
                                  onsubmit="deleteValue()">
                                <input type="text" name="search" class="number_delivery" placeholder="# invoice" required>
                                <button class="btn btn-success" type="submit">Найти</button>
                                <hr>
                            </form>
                        </div>
                    </div>
                </div>-->
            <?php if($data['search'] === true){?>
            <div class="col-md-12">
                <div class="box box-widget">
                    <div class="box-body">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="row">
                                    <form action="/edit" method="post">
                                    <div class="col-md-3 mb-3">
                                        <label for="first_name">Имя</label>
                                        <input class="form-control" id="first_name" name="first_name" placeholder="" value="<?=$data['data']['recipient_firstname'];?>" required=""
                                               type="text">
                                    </div>
                                    <div class="col-md-3 mb-3">
                                        <label for="surname">Фамилия</label>
                                        <input class="form-control" id="surname" name="surname" placeholder="" value="<?=$data['data']['recipient_lastname'];?>" required=""
                                               type="text">
                                    </div>
                                    <div class="col-md-3 mb-3">
                                        <label for="phone">Телефон</label>
                                        <input class="form-control" id="phone" name="phone" placeholder="" value="<?=$data['data']['phone'];?>" required=""
                                               type="text">
                                    </div>
                                    <div class="col-md-3 mb-3">
                                        <label for="city">Город</label>
                                        <input class="form-control" id="city" name="city" placeholder="" value="<?=$data['data']['city'];?>" required=""
                                               type="text">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-3 mb-3">
                                        <label for="secession">Отделение</label>
                                        <input class="form-control" id="secession" name="secession" placeholder="" value="<?=$data['data']['secession'];?>" required=""
                                               type="text">
                                    </div>
                                    <div class="col-md-3 mb-3">
                                        <label for="count_del">Количество мест</label>
                                        <input class="form-control" id="count_del" name="count_del" placeholder="" value="<?=$data['data']['count_delivery'];?>" readonly
                                               type="text">
                                    </div>
                                    <div class="col-md-3 mb-3">
                                        <label for="weight">Вес</label>
                                        <input class="form-control" id="weight" name="weight" placeholder="" value="<?=$data['data']['width'];?>" required=""
                                               type="text">
                                        <input class="form-control" hidden="hidden" style="display: none;" id="id_del" name="id_del" placeholder="" value="<?=$data['data']['id'];?>" required=""
                                               type="text">
                                        <input class="form-control" hidden="hidden" style="display: none;" id="num_del" name="num_del" placeholder="" value="<?=$data['data']['inv_china'][0];?>" required=""
                                               type="text">
                                    </div>
                                    <div class="col-md-3 mb-3">
                                        <label for="cost">Цена</label>
                                        <input class="form-control" id="cost" name="cost" placeholder="" value="<?=$data['data']['price'];?>" required=""
                                               type="text">
                                    </div>
                                </div>
                                <div class="row">
                                    <button class="btn btn-primary mt-10" type="submit" name="edit_del"  style="margin-left: 14px;margin-top: 5px;">Обновить</button>
                                    </form>
                                    <form action="/edit" method="post">
                                        <button class="btn btn-primary mt-10" type="submit" name="search_back"  style="margin-left: 14px;margin-top: 5px;">Вернутся к поиску</button>
                                    </form>
                                </div>
                                <div class="row">
                                    <div class="col-4">
                                        <?php if($data['update'] === true):?>
                                            <div class="alert alert-success" role="alert" style="margin-top: 10px;">
                                                Данные успешно обновлены
                                            </div>
                                        <?php endif;?>
                                    </div>
                                </div>


                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /.col -->
            <?php } else{?>
                <div class="col-md-6">
                    <div class="box box-widget">
                        <div class="box-header with-border">
                            <h3 class="box-title">Введите номер накладной</h3>
                        </div>
                        <div class="box-body">
                            <form method="post" action="/edit" class="form-search form-inline"
                                  onsubmit="deleteValue()">
                                <input type="text" name="search" class="number_delivery" placeholder="# invoice" required>
                                <button class="btn btn-success" type="submit">Найти</button>
                                <hr>
                            </form>
                            <?php if(isset($data['errors'])):?>
                                <div class="alert alert-danger" role="alert" style="margin-top: 10px;">
                                    Номер не найден
                                </div>
                            <?php endif;?>
                        </div>
                    </div>
                </div>
            <?}?>
        </div>
        <!-- /.row -->


    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->