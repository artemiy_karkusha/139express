<div class="content-wrapper">
    <? if ($data['userLaw'] == 3) { ?>
    <section class="content-header">
        <h1>
            Edit user
        </h1>
        <ol class="breadcrumb">
            <li><a href="/admin"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">User add</li>
        </ol>
    </section>
    <section class="content">
    <div class="row">
                <?if($data['update_user']):?>
                    <div class="alert alert-success" role="alert">
                        User successfully update
                    </div>
                <?endif;?>
                <?if($data['update_user'] === FALSE):?>
                    <div class="alert alert-danger" role="alert">
                        User not update
                    </div>
                <?endif;?>
                <?if($data['delete_user']):?>
                    <div class="alert alert-success" role="alert">
                        User successfully delete
                    </div>
                <?endif;?>
                <?if($data['delete_user'] === FALSE):?>
                    <div class="alert alert-danger" role="alert">
                        User not delete
                    </div>
                <?endif;?>
            </div>
            <?=$data['allCard'];?>
        <?}else{?>
            <div class="page-title">
                <div>
                    <h1 style="color:red">You don't have accesss this page</h1>
                </div>
                <div>
                    <ul class="breadcrumb">
                        <li><i class="fa fa-home fa-lg"></i></li>
                        <li><a href="/admin">Main</a></li>
                    </ul>
                </div>
            </div>
        <?}?>
    </div>
</div>
