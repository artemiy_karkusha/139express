<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <section class="content-header">
        <h1>
            Оформить
        </h1>
        <ol class="breadcrumb">
            <li><a href="/admin"><i class="fa fa-dashboard"></i>Главная</a></li>
            <li class="active">Оформить</li>
        </ol>
    </section>
    <section class="content">

        <div class="row">
            <div class="col-md-4">
                <div class="box box-widget">
                    <form method="post" action="/novaposhta_issue">
                        <div class="row"  style="margin-left: 10px;padding-top: 10px;margin-right: 10px;">
                            <div class="col-md-12 mb-3">
                                <label for="first_name">Оформить все доставки</label>
                                <button class="btn btn-primary mt-10" type="submit" name="delivery_issue"  style="margin-left: 23px;margin-bottom: 10px;margin-top: 10px">Оформить</button>
                            </div>

                        </div>
                        <div class="row"  style="margin-left:5px;margin-right: 5px;">
                            <div class="col-md-12 mb-3">
                                <?php if ($data['delivery_issue']) : ?>
                                    <div class="alert alert-success mt-5" role="alert">
                                        Оформлено <?=$data['delivery_issue'];?> доставок
                                    </div>
                                <?php endif; ?>
                                <?php if ($data['delivery_issue_null']) : ?>
                                    <div class="alert alert-warning mt-5" role="alert">
                                        <?=$data['delivery_issue_null'];?>
                                    </div>
                                <?php endif; ?>
                            </div>
                        </div>

                    </form>
                </div>
            </div>
            <div class="col-md-4">
                <div class="box box-widget">
                    <div class="row"  style="margin-left: 10px;padding-top: 10px;margin-right: 10px;">
                        <form method="post" action="/novaposhta_issue">
                        <div class="col-md-12 mb-3">
                            <label for="first_name">Выгрузить проблемные</label>
                            <button class="btn btn-primary mt-10" type="submit" name="errorsDeliveryExcel"  style="margin-left: 23px;margin-bottom: 10px;margin-top: 10px">Выполнить</button>
                            <?php if ($data['reportExcel']) : ?>
                                <div class="alert alert-warning mt-5" role="alert">
                                    <?=$data['reportExcel'];?>
                                </div>
                            <?php endif; ?>
                        </div>
                        </form>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="box box-widget">
                    <div class="row"  style="margin-left: 10px;padding-top: 10px;margin-right: 10px;">
                        <div class="col-md-12 mb-3">
                            <label for="first_name">В разработке...</label>
                        </div>

                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="box box-widget">
                    <table class="table table-hover">
                        <thead>
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">Номер накладной</th>
                            <th scope="col">Проблема</th>
                            <th scope="col">Дейстие</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?=$data['table']?>
                        </tbody>
                    </table>
                </div>

            </div>

            <div
                    class="ldBar label-center loader"
                    id="loader"
                    style="display: none;"
                    data-value="0"
                    data-preset="bubble"
            ></div>
        </div>

</div>

<!-- /.content-wrapper -->
