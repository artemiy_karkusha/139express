<div class="content-wrapper">
    <? if ($data['userLaw'] == 3) { ?>
        <section class="content-header">
            <h1>
                Добавить нового пользователя
            </h1>
            <ol class="breadcrumb">
                <li><a href="/admin"><i class="fa fa-dashboard"></i> Главная</a></li>
                <li class="active">Добавить пользователя</li>
            </ol>
        </section>
    <section class="content">

        <div class="row">
            <div class="col-md-12">
                <div class="box box-widget">
                <form method="post" action="/user_add">
                    <div class="row"  style="margin-left: 10px;padding-top: 10px;margin-right: 10px;">
                        <div class="col-md-4 mb-3">
                            <label for="first_name">Имя</label>
                            <input type="text" class="form-control" id="first_name" name="first_name"
                                   placeholder="Имя" value="" required>
                        </div>
                        <div class="col-md-4 mb-3">
                            <label for="last_name">Фамилия</label>
                            <input type="text" class="form-control" id="last_name" name="last_name"
                                   placeholder="Фамилия" value="" required>
                        </div>
                        <div class="col-md-4 mb-3">
                            <label for="login">Логин</label>
                            <input type="text" class="form-control" id="login" name="login" placeholder="Логин"
                                   aria-describedby="inputGroupPrepend" required>
                        </div>
                    </div>
                    <div class="row"  style="margin: 10px">
                        <div class="col-md-6 mb-3">
                            <label for="password">Пароль</label>
                            <input type="text" class="form-control" id="password" name="password" placeholder="Пароль"
                                   required>
                        </div>
                        <div class="col-md-3 mb-3">
                            <label for="law">Права доступа</label>
                            <select class="form-control" id="law" name="law" required="">
                                <option value="level_2" selected>User</option>
                                <option value="level_3" selected>Admin</option>
                            </select>
                        </div>
                    </div>
                    <button class="btn btn-primary mt-10" type="submit" name="add_user"  style="margin-left: 23px;margin-bottom: 10px">Добавить</button>
                </form>
                </div>
            </div>
        </div>
                    <? if ($data['add_user']) { ?>
                        <div class="alert alert-success mt-10" role="alert" style="margin-top: 20px;">
                            Пользователь успешно создан
                        </div>
                    <? } ?>
    <? } else { ?>
        <div class="page-title">
            <div>
                <h1 style="color:red">Вы не имеете доступ к этой странице</h1>
            </div>
            <div>
                <ul class="breadcrumb">
                    <li><i class="fa fa-home fa-lg"></i></li>
                    <li><a href="/admin">Main</a></li>
                </ul>
            </div>
        </div>
    <? } ?>
    </section>
</div>
