
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Statistic
        </h1>
        <ol class="breadcrumb">
            <li><a href="/admin"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Novaposhta</li>
        </ol>
    </section>
    <!-- Main content -->
    <section class="content">
    <? if ($data['userLaw'] == 3) { ?>
        <div class="row">
            <div class="col-md-12">
                <?php include("seestats.php"); ?>
            </div>
        </div>
    <? } else { ?>
        <div class="page-title">
            <div>
                <h1 style="color:red">You don't have accesss this page</h1>
            </div>
            <div>
                <ul class="breadcrumb">
                    <li><i class="fa fa-home fa-lg"></i></li>
                    <li><a href="/admin">Main</a></li>
                </ul>
            </div>
        </div>
    <? } ?>
</div>
</div>
