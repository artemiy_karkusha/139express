<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <section class="content-header">
        <h1>
            Уведомления
        </h1>
        <ol class="breadcrumb">
            <li><a href="/admin"><i class="fa fa-dashboard"></i>Главная</a></li>
            <li class="active">Уведомления</li>
        </ol>
    </section>
    <section class="content">

        <div class="row">
            <div class="col-md-4">
                <div class="box box-widget">
                    <form method="post" action="/notification">
                        <div class="row"  style="margin-left: 10px;padding-top: 10px;margin-right: 10px;">
                            <div class="col-md-12 mb-3">
                                <label for="first_name">Отправить смс-уведомление</label>
                                <button class="btn btn-primary mt-10" type="submit" name="sms_send"  style="margin-left: 23px;margin-bottom: 10px;margin-top: 10px">Отправить</button>
                            </div>

                        </div>
                        <div class="row"  style="margin-left:5px;margin-right: 5px;">
                            <div class="col-md-12 mb-3">
                                <?php if ($data['sms_send']) : ?>
                                    <div class="alert alert-success mt-5" role="alert">
                                        <?=$data['sms_send'];?>
                                    </div>
                                <?php endif; ?>
                            </div>
                        </div>

                    </form>
                </div>
            </div>
            <div class="col-md-4">
                <div class="box box-widget">
                    <div class="row"  style="margin-left: 10px;padding-top: 10px;margin-right: 10px;">
                        <div class="col-md-12 mb-3">
                            <label for="first_name">В разработке...</label>
                        </div>

                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="box box-widget">
                    <div class="row"  style="margin-left: 10px;padding-top: 10px;margin-right: 10px;">
                        <div class="col-md-12 mb-3">
                            <label for="first_name">В разработке...</label>
                        </div>

                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="box box-widget">
                    <table class="table table-hover">
                        <thead>
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">ПІБ</th>
                            <th scope="col">Телефон</th>
                            <th scope="col">Дейстие</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?=$data['table']?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
</div>
<!-- /.content-wrapper -->