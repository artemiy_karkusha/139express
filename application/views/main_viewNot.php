

<!--================Main Slider Area =================-->
<section class="main_slider_area">
    <div id="main_slider" class="rev_slider" data-version="5.3.1.6">
        <ul>
            <li data-index="rs-2972" data-transition="fade" data-slotamount="default" data-hideafterloop="0" data-hideslideonmobile="off" data-easein="default" data-easeout="default" data-masterspeed="300" data-rotate="0" data-saveperformance="off" data-title="Creative"
                data-param1="01" data-param2="" data-param3="" data-param4="" data-param5="" data-param6="" data-param7="" data-param8="" data-param9="" data-param10="" data-description="">
                <!-- MAIN IMAGE -->
                <img src="img/home-slider/slider-1.jpg" alt="" data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" data-bgparallax="5" class="rev-slidebg" data-no-retina>
                <!-- LAYERS -->
                <div class="slider_text_box">
                    <div class="tp-caption first_text" data-x="['left','left','left','center']" data-y="['top','top','top','top']" data-hoffset="['0','0','0','0']" data-voffset="['250','250','250','140']" data-fontsize="['34','34','34','20']" data-lineheight="['44','44','44','30']"
                         data-width="none" data-height="none" data-whitespace="nowrap" data-type="text" data-responsive_offset="on" data-frames="[{&quot;delay&quot;:10,&quot;speed&quot;:1500,&quot;frame&quot;:&quot;0&quot;,&quot;from&quot;:&quot;y:[-100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;&quot;,&quot;mask&quot;:&quot;x:0px;y:0px;s:inherit;e:inherit;&quot;,&quot;to&quot;:&quot;o:1;&quot;,&quot;ease&quot;:&quot;Power2.easeInOut&quot;},{&quot;delay&quot;:&quot;wait&quot;,&quot;speed&quot;:1500,&quot;frame&quot;:&quot;999&quot;,&quot;to&quot;:&quot;y:[175%];&quot;,&quot;mask&quot;:&quot;x:inherit;y:inherit;s:inherit;e:inherit;&quot;,&quot;ease&quot;:&quot;Power2.easeInOut&quot;}]"
                         data-textAlign="['left','left','left','left']" data-paddingtop="[0,0,0,0]" data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]">Reliable and flexible Logistics</div>

                    <div class="tp-caption secand_text" data-x="['left','left','left','center']" data-y="['top','top','top','top']" data-hoffset="['0','0','0','0']" data-voffset="['315','315','315','190']" data-fontsize="['50','50','50','30']" data-lineheight="['60','60','60','40']"
                         data-width="['730','730','730','400']" data-height="none" data-whitespace="['normal','normal','normal','normal']" data-type="text" data-responsive_offset="on" data-frames="[{&quot;delay&quot;:10,&quot;speed&quot;:1500,&quot;frame&quot;:&quot;0&quot;,&quot;from&quot;:&quot;y:[-100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;&quot;,&quot;mask&quot;:&quot;x:0px;y:0px;s:inherit;e:inherit;&quot;,&quot;to&quot;:&quot;o:1;&quot;,&quot;ease&quot;:&quot;Power2.easeInOut&quot;},{&quot;delay&quot;:&quot;wait&quot;,&quot;speed&quot;:1500,&quot;frame&quot;:&quot;999&quot;,&quot;to&quot;:&quot;y:[175%];&quot;,&quot;mask&quot;:&quot;x:inherit;y:inherit;s:inherit;e:inherit;&quot;,&quot;ease&quot;:&quot;Power2.easeInOut&quot;}]"
                         data-textAlign="['left','left','left','center']" data-paddingtop="[0,0,0,0]" data-paddingright="[0,0,0,0]" data-paddingbottom="[10,10,10,10]" data-paddingleft="[0,0,0,0]">Personalized cargo services in worldwide commerce</div>

                    <div class="tp-caption slider_btn" data-x="['left','left','left','center']" data-y="['top','top','top','top']" data-hoffset="['0','0','0','0']" data-voffset="['470','470','470','335']" data-fontsize="['16','16','16','16']" data-lineheight="['26','26','26','26']"
                         data-width="none" data-height="none" data-whitespace="nowrap" data-type="text" data-responsive_offset="on" data-frames='[{"from":"y:[-100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;","mask":"x:0px;y:0px;s:inherit;e:inherit;","speed":1500,"to":"o:1;","delay":150,"ease":"Power2.easeInOut"},{"delay":"wait","speed":300,"to":"opacity:0;","ease":"nothing"}]'
                         data-textAlign="['left','left','left','left']" data-paddingtop="[0,0,0,0]" data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]"><a class="quote_btn" href="#">KNOW MORE <i class="fa fa-angle-right"></i></a></div>

                    <div class="tp-caption ContentZoom-SmallIcon tp-resizeme rs-parallaxlevel-0" data-x="['left','left','left','center']" data-y="['top','top','top','top']" data-hoffset="['0','0','0','-25']" data-voffset="['190','190','190','80']" data-width="none" data-height="none"
                         data-whitespace="nowrap" data-transform_idle="o:1;" data-transform_hover="o:1;rX:0;rY:0;rZ:0;z:0;s:300;e:Power1.easeInOut;" data-style_hover="c:rgba(111, 124, 130, 1.00);cursor:pointer;" data-transform_in="opacity:0;s:500;e:Power1.easeInOut;"
                         data-transform_out="opacity:0;s:500;e:Power1.easeInOut;s:500;e:Power1.easeInOut;" data-start="0" data-splitin="none" data-splitout="none" data-actions='[{"event":"click","action":"jumptoslide","slide":"previous","delay":""}]' data-responsive_offset="on"><i class="fa fa-angle-left"></i>
                    </div>
                    <div class="tp-caption ContentZoom-SmallIcon tp-resizeme rs-parallaxlevel-0" data-x="['left','left','left','center']" data-y="['top','top','top','top']" data-hoffset="['50','50','50','25']" data-voffset="['190','190','190','80']" data-width="none" data-height="none"
                         data-whitespace="nowrap" data-transform_idle="o:1;" data-transform_hover="o:1;rX:0;rY:0;rZ:0;z:0;s:300;e:Power1.easeInOut;" data-style_hover="c:rgba(111, 124, 130, 1.00);cursor:pointer;" data-transform_in="opacity:0;s:500;e:Power1.easeInOut;"
                         data-transform_out="opacity:0;s:500;e:Power1.easeInOut;s:500;e:Power1.easeInOut;" data-start="0" data-splitin="none" data-splitout="none" data-actions='[{"event":"click","action":"jumptoslide","slide":"next","delay":""}]' data-responsive_offset="on"><i class="fa fa-angle-right"></i>
                    </div>
                </div>
            </li>
            <li data-index="rs-2973" data-transition="fade" data-slotamount="default" data-hideafterloop="0" data-hideslideonmobile="off" data-easein="default" data-easeout="default" data-masterspeed="300" data-rotate="0" data-saveperformance="off" data-title="Creative"
                data-param1="01" data-param2="" data-param3="" data-param4="" data-param5="" data-param6="" data-param7="" data-param8="" data-param9="" data-param10="" data-description="">
                <!-- MAIN IMAGE -->
                <img src="img/home-slider/slider-2.jpg" alt="" data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" data-bgparallax="5" class="rev-slidebg" data-no-retina>
                <!-- LAYERS -->
                <div class="slider_text_box">
                    <div class="tp-caption first_text" data-x="['left','left','left','center']" data-y="['top','top','top','top']" data-hoffset="['0','0','0','0']" data-voffset="['250','250','250','140']" data-fontsize="['34','34','34','20']" data-lineheight="['44','44','44','30']"
                         data-width="none" data-height="none" data-whitespace="nowrap" data-type="text" data-responsive_offset="on" data-frames="[{&quot;delay&quot;:10,&quot;speed&quot;:1500,&quot;frame&quot;:&quot;0&quot;,&quot;from&quot;:&quot;y:[-100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;&quot;,&quot;mask&quot;:&quot;x:0px;y:0px;s:inherit;e:inherit;&quot;,&quot;to&quot;:&quot;o:1;&quot;,&quot;ease&quot;:&quot;Power2.easeInOut&quot;},{&quot;delay&quot;:&quot;wait&quot;,&quot;speed&quot;:1500,&quot;frame&quot;:&quot;999&quot;,&quot;to&quot;:&quot;y:[175%];&quot;,&quot;mask&quot;:&quot;x:inherit;y:inherit;s:inherit;e:inherit;&quot;,&quot;ease&quot;:&quot;Power2.easeInOut&quot;}]"
                         data-textAlign="['left','left','left','left']" data-paddingtop="[0,0,0,0]" data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]">Reliable and flexible Logistics</div>

                    <div class="tp-caption secand_text" data-x="['left','left','left','center']" data-y="['top','top','top','top']" data-hoffset="['0','0','0','0']" data-voffset="['315','315','315','190']" data-fontsize="['50','50','50','30']" data-lineheight="['60','60','60','40']"
                         data-width="['730','730','730','400']" data-height="none" data-whitespace="['normal','normal','normal','normal']" data-type="text" data-responsive_offset="on" data-frames="[{&quot;delay&quot;:10,&quot;speed&quot;:1500,&quot;frame&quot;:&quot;0&quot;,&quot;from&quot;:&quot;y:[-100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;&quot;,&quot;mask&quot;:&quot;x:0px;y:0px;s:inherit;e:inherit;&quot;,&quot;to&quot;:&quot;o:1;&quot;,&quot;ease&quot;:&quot;Power2.easeInOut&quot;},{&quot;delay&quot;:&quot;wait&quot;,&quot;speed&quot;:1500,&quot;frame&quot;:&quot;999&quot;,&quot;to&quot;:&quot;y:[175%];&quot;,&quot;mask&quot;:&quot;x:inherit;y:inherit;s:inherit;e:inherit;&quot;,&quot;ease&quot;:&quot;Power2.easeInOut&quot;}]"
                         data-textAlign="['left','left','left','center']" data-paddingtop="[0,0,0,0]" data-paddingright="[0,0,0,0]" data-paddingbottom="[10,10,10,10]" data-paddingleft="[0,0,0,0]">Personalized cargo services in worldwide commerce</div>

                    <div class="tp-caption slider_btn" data-x="['left','left','left','center']" data-y="['top','top','top','top']" data-hoffset="['0','0','0','0']" data-voffset="['470','470','470','335']" data-fontsize="['16','16','16','16']" data-lineheight="['26','26','26','26']"
                         data-width="none" data-height="none" data-whitespace="nowrap" data-type="text" data-responsive_offset="on" data-frames='[{"from":"y:[-100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;","mask":"x:0px;y:0px;s:inherit;e:inherit;","speed":1500,"to":"o:1;","delay":150,"ease":"Power2.easeInOut"},{"delay":"wait","speed":300,"to":"opacity:0;","ease":"nothing"}]'
                         data-textAlign="['left','left','left','left']" data-paddingtop="[0,0,0,0]" data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]"><a class="quote_btn" href="#">KNOW MORE <i class="fa fa-angle-right"></i></a></div>

                    <div class="tp-caption ContentZoom-SmallIcon tp-resizeme rs-parallaxlevel-0" data-x="['left','left','left','center']" data-y="['top','top','top','top']" data-hoffset="['0','0','0','-25']" data-voffset="['190','190','190','80']" data-width="none" data-height="none"
                         data-whitespace="nowrap" data-transform_idle="o:1;" data-transform_hover="o:1;rX:0;rY:0;rZ:0;z:0;s:300;e:Power1.easeInOut;" data-style_hover="c:rgba(111, 124, 130, 1.00);cursor:pointer;" data-transform_in="opacity:0;s:500;e:Power1.easeInOut;"
                         data-transform_out="opacity:0;s:500;e:Power1.easeInOut;s:500;e:Power1.easeInOut;" data-start="0" data-splitin="none" data-splitout="none" data-actions='[{"event":"click","action":"jumptoslide","slide":"previous","delay":""}]' data-responsive_offset="on"><i class="fa fa-angle-left"></i>
                    </div>
                    <div class="tp-caption ContentZoom-SmallIcon tp-resizeme rs-parallaxlevel-0" data-x="['left','left','left','center']" data-y="['top','top','top','top']" data-hoffset="['50','50','50','25']" data-voffset="['190','190','190','80']" data-width="none" data-height="none"
                         data-whitespace="nowrap" data-transform_idle="o:1;" data-transform_hover="o:1;rX:0;rY:0;rZ:0;z:0;s:300;e:Power1.easeInOut;" data-style_hover="c:rgba(111, 124, 130, 1.00);cursor:pointer;" data-transform_in="opacity:0;s:500;e:Power1.easeInOut;"
                         data-transform_out="opacity:0;s:500;e:Power1.easeInOut;s:500;e:Power1.easeInOut;" data-start="0" data-splitin="none" data-splitout="none" data-actions='[{"event":"click","action":"jumptoslide","slide":"next","delay":""}]' data-responsive_offset="on"><i class="fa fa-angle-right"></i>
                    </div>
                </div>
            </li>
        </ul>
    </div>
</section>
<!--================End Main Slider Area =================-->

<!--================Feature Slider Area =================-->
<section class="feature_slider_area">
    <div class="container">
        <div class="row feature_row">
            <div class="feature_slider_inner owl-carousel">
                <div class="item">
                    <div class="feature_s_item">
                        <img alt="" class="svg social-link" src="img/icon/svg/aeroplane.svg" />
                        <a href="#">
                            <h4>Air Cargo</h4>
                        </a>
                        <p>Phosfluorescently enhance 24/7 resources through distributed materials. Distinctively incubate cross-media bandwidth for sticky platforms.</p>
                    </div>
                </div>
                <div class="item">
                    <div class="feature_s_item">
                        <img alt="" class="svg social-link" src="img/icon/svg/ship.svg" />
                        <a href="#">
                            <h4>Sea Freight</h4>
                        </a>
                        <p>Phosfluorescently enhance 24/7 resources through distributed materials. Distinctively incubate cross-media bandwidth for sticky platforms.</p>
                    </div>
                </div>
                <div class="item">
                    <div class="feature_s_item">
                        <img alt="" class="svg social-link" src="img/icon/svg/delivery-truck.svg" />
                        <a href="#">
                            <h4>Road Transport</h4>
                        </a>
                        <p>Phosfluorescently enhance 24/7 resources through distributed materials. Distinctively incubate cross-media bandwidth for sticky platforms.</p>
                    </div>
                </div>
                <div class="item">
                    <div class="feature_s_item">
                        <img alt="" class="svg social-link" src="img/icon/svg/aeroplane.svg" />
                        <a href="#">
                            <h4>Air Cargo</h4>
                        </a>
                        <p>Phosfluorescently enhance 24/7 resources through distributed materials. Distinctively incubate cross-media bandwidth for sticky platforms.</p>
                    </div>
                </div>
                <div class="item">
                    <div class="feature_s_item">
                        <img alt="" class="svg social-link" src="img/icon/svg/ship.svg" />
                        <a href="#">
                            <h4>Sea Freight</h4>
                        </a>
                        <p>Phosfluorescently enhance 24/7 resources through distributed materials. Distinctively incubate cross-media bandwidth for sticky platforms.</p>
                    </div>
                </div>
                <div class="item">
                    <div class="feature_s_item">
                        <img alt="" class="svg social-link" src="img/icon/svg/delivery-truck.svg" />
                        <a href="#">
                            <h4>Road Transport</h4>
                        </a>
                        <p>Phosfluorescently enhance 24/7 resources through distributed materials. Distinctively incubate cross-media bandwidth for sticky platforms.</p>
                    </div>
                </div>
                <div class="item">
                    <div class="feature_s_item">
                        <img alt="" class="svg social-link" src="img/icon/svg/aeroplane.svg" />
                        <a href="#">
                            <h4>Air Cargo</h4>
                        </a>
                        <p>Phosfluorescently enhance 24/7 resources through distributed materials. Distinctively incubate cross-media bandwidth for sticky platforms.</p>
                    </div>
                </div>
                <div class="item">
                    <div class="feature_s_item">
                        <img alt="" class="svg social-link" src="img/icon/svg/ship.svg" />
                        <a href="#">
                            <h4>Sea Freight</h4>
                        </a>
                        <p>Phosfluorescently enhance 24/7 resources through distributed materials. Distinctively incubate cross-media bandwidth for sticky platforms.</p>
                    </div>
                </div>
                <div class="item">
                    <div class="feature_s_item">
                        <img alt="" class="svg social-link" src="img/icon/svg/delivery-truck.svg" />
                        <a href="#">
                            <h4>Road Transport</h4>
                        </a>
                        <p>Phosfluorescently enhance 24/7 resources through distributed materials. Distinctively incubate cross-media bandwidth for sticky platforms.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!--================End Feature Slider Area =================-->

<!--================Main Feature Area =================-->
<section class="main_feature_area">
    <div class="left_feature_content">
        <div class="left_feature_inner_text">
            <div class="media">
                <div class="media-left">
                    <img class="svg social-link" src="img/icon/svg/pointer.svg" alt="" />
                </div>
                <div class="media-body">
                    <a href="#">
                        <h4>Online Booking</h4>
                    </a>
                    <p>Contrary to popular belief, Lorem Ipsum is not simply random text. </p>
                </div>
            </div>
            <div class="media">
                <div class="media-left">
                    <img class="svg social-link" src="img/icon/svg/warehouse.svg" alt="" />
                </div>
                <div class="media-body">
                    <a href="#">
                        <h4>Online Booking</h4>
                    </a>
                    <p>Contrary to popular belief, Lorem Ipsum is not simply random text. </p>
                </div>
            </div>
            <div class="media">
                <div class="media-left">
                    <img class="svg social-link" src="img/icon/svg/route.svg" alt="" />
                </div>
                <div class="media-body">
                    <a href="#">
                        <h4>Online Booking</h4>
                    </a>
                    <p>Contrary to popular belief, Lorem Ipsum is not simply random text. </p>
                </div>
            </div>
            <div class="media">
                <div class="media-left">
                    <img class="svg social-link" src="img/icon/svg/delivery-truck2.svg" alt="" />
                </div>
                <div class="media-body">
                    <a href="#">
                        <h4>Online Booking</h4>
                    </a>
                    <p>Contrary to popular belief, Lorem Ipsum is not simply random text. </p>
                </div>
            </div>
            <div class="media">
                <div class="media-left">
                    <img class="svg social-link" src="img/icon/svg/flight.svg" alt="" />
                </div>
                <div class="media-body">
                    <a href="#">
                        <h4>Online Booking</h4>
                    </a>
                    <p>Contrary to popular belief, Lorem Ipsum is not simply random text. </p>
                </div>
            </div>
            <div class="media">
                <div class="media-left">
                    <img class="svg social-link" src="img/icon/svg/telemarketer.svg" alt="" />
                </div>
                <div class="media-body">
                    <a href="#">
                        <h4>Online Booking</h4>
                    </a>
                    <p>Contrary to popular belief, Lorem Ipsum is not simply random text. </p>
                </div>
            </div>
        </div>
    </div>
    <div class="right_feature_image">
        <img src="img/feature-right.jpg" alt="">
    </div>
</section>
<!--================End Main Feature Area =================-->

<!--================Our Blog Area =================-->
<section class="our_blog_area">
    <div class="container">
        <div class="main_title">
            <h5>LATEST NEWS</h5>
            <h2>FROM OUR BLOG</h2>
        </div>
        <div class="row our_blog_inner">
            <div class="col-md-4">
                <div class="our_blog_item">
                    <div class="our_blog_img">
                        <img src="img/blog/our-blog/our-blog-1.jpg" alt="">
                        <div class="b_date">
                            <h6>JULY</h6>
                            <h5>25</h5>
                        </div>
                    </div>
                    <div class="our_blog_content">
                        <a href="blog_details_view.php">
                            <h4>QUALITY CARGO SERVICES AT AFFORDABLE PRICE</h4>
                        </a>
                        <p>Dramatically transition clicks-and-mortar portals before diverse leadership skills. Monotonectally recaptiualize fully tested information after.</p>
                        <h6><a href="#">Frank Martin</a><span>•</span><a href="#">9 Comments</a></h6>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="our_blog_item">
                    <div class="our_blog_img">
                        <img src="img/blog/our-blog/our-blog-2.jpg" alt="">
                        <div class="b_date">
                            <h6>JULY</h6>
                            <h5>12</h5>
                        </div>
                    </div>
                    <div class="our_blog_content">
                        <a href="blog_details_view.php">
                            <h4>WHY CHOOSE OUR WAREHOUSING SERVICE?</h4>
                        </a>
                        <p>Dramatically transition clicks-and-mortar portals before diverse leadership skills. Monotonectally recaptiualize fully tested information after.</p>
                        <h6><a href="#">Frank Martin</a><span>•</span><a href="#">9 Comments</a></h6>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="our_blog_item">
                    <div class="our_blog_img">
                        <img src="img/blog/our-blog/our-blog-3.jpg" alt="">
                        <div class="b_date">
                            <h6>June</h6>
                            <h5>07</h5>
                        </div>
                    </div>
                    <div class="our_blog_content">
                        <a href="blog_details_view.php">
                            <h4>TOP BENEFITS OF HIRING OUR <br class="b_title_br" />TRUCKING SERVICE</h4>
                        </a>
                        <p>Dramatically transition clicks-and-mortar portals before diverse leadership skills. Monotonectally recaptiualize fully tested information after.</p>
                        <h6><a href="#">Frank Martin</a><span>•</span><a href="#">9 Comments</a></h6>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!--================End Our Blog Area =================-->

<!--================Get Quote Area =================-->
<section class="get_quote_area">
    <div class="container">
        <div class="pull-left">
            <h4>Providing Smart Logistic Solution Across The World</h4>
        </div>
        <div class="pull-right">
            <a class="b_get_btn" href="request_quote_view.php">Get a Quote <i class="fa fa-angle-right"></i></a>
        </div>
    </div>
</section>
<!--================End Get Quote Area =================-->

<!--================Testimonials Area =================-->
<section class="testimonials_area">
    <div class="container">
        <div class="testimonials_inner">
            <div class="testimonials_slider owl-carousel">
                <div class="item">
                    <img class="img-circle" src="img/testimonials/testimonials-1.png" alt="">
                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type
                        specimen book.</p>
                    <h4>Steve Jobs</h4>
                </div>
                <div class="item">
                    <img class="img-circle" src="img/testimonials/testimonials-1.png" alt="">
                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type
                        specimen book.</p>
                    <h4>Jon Anderson</h4>
                </div>
                <div class="item">
                    <img class="img-circle" src="img/testimonials/testimonials-1.png" alt="">
                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type
                        specimen book.</p>
                    <h4>Steve Jobs</h4>
                </div>
            </div>
        </div>
    </div>
</section>
<!--================End Testimonials Area =================-->

<!--================About Us Area =================-->
<section class="about_area">
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <div class="about_left_text">
                    <h3 class="single_title">About Us</h3>
                    <p>Established in the United States of America, Start.ly has operations all over the world. Its regional headquarter is located in Dubai which serves as the hub of Western Asia. We have in place a wide network of our own international
                        offices and affiliates dedicated around the world.</p>
                    <p>Globally repurpose distinctive "outside the box" thinking and premium process improvements. Dynamically utilize flexible ideas after future-proof expertise. Progressively disseminate multidisciplinary relationships.
                    </p>
                    <a class="more_btn" href="#">KNOW MORE <i class="fa fa-angle-right"></i></a>
                </div>
            </div>
            <div class="col-md-6">
                <h4 class="single_title">OUR LOCATIONS</h4>
                <!-- Configure Your Global Locations in /js/locations.js -->
                <div id="jvectormap"></div>
            </div>
        </div>
    </div>
</section>
<!--================End About Us Area =================-->

<!--================Gallery Area =================-->
<section class="gallery_area">
    <div class="container">
        <div class="col-md-7">
            <div class="left_gallery_area">
                <h3 class="single_title">Gallery</h3>
                <div class="l_gallery_inner zoom-gallery">
                    <div class="l_gallery_item">
                        <a href="img/gallery/gallery-big-1.jpg">
                            <img src="img/gallery/gallery-1.jpg" alt="">
                        </a>
                    </div>
                    <div class="l_gallery_item">
                        <a href="img/gallery/gallery-big-2.jpg">
                            <img src="img/gallery/gallery-2.jpg" alt="">
                        </a>
                    </div>
                    <div class="l_gallery_item">
                        <a href="img/gallery/gallery-big-3.jpg">
                            <img src="img/gallery/gallery-3.jpg" alt="">
                        </a>
                    </div>
                    <div class="l_gallery_item">
                        <a href="img/gallery/gallery-big-4.jpg">
                            <img src="img/gallery/gallery-4.jpg" alt="">
                        </a>
                    </div>
                    <div class="l_gallery_item">
                        <a href="img/gallery/gallery-big-5.jpg">
                            <img src="img/gallery/gallery-5.jpg" alt="">
                        </a>
                    </div>
                    <div class="l_gallery_item">
                        <a href="img/gallery/gallery-big-6.jpg">
                            <img src="img/gallery/gallery-6.jpg" alt="">
                        </a>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-5">
            <div class="right_faq_area">
                <h3 class="single_title">FAQ</h3>
                <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                    <div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="headingOne">
                            <h4 class="panel-title">
                                <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                    How can our clients track the shipments?
                                </a>
                            </h4>
                        </div>
                        <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
                            <div class="panel-body">
                                Collaboratively incubate ubiquitous e-business without enterprise ideas. Objectively seize 2.0 quality vectors before stand-alone
                            </div>
                        </div>
                    </div>
                    <div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="headingTwo">
                            <h4 class="panel-title">
                                <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                    What kind of insurance do you provide for the packages ?
                                </a>
                            </h4>
                        </div>
                        <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
                            <div class="panel-body">
                                Collaboratively incubate ubiquitous e-business without enterprise ideas. Objectively seize 2.0 quality vectors before stand-alone
                            </div>
                        </div>
                    </div>
                    <div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="headingThree">
                            <h4 class="panel-title">
                                <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                    How can I make payments, What are your refund rules?
                                </a>
                            </h4>
                        </div>
                        <div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
                            <div class="panel-body">
                                Collaboratively incubate ubiquitous e-business without enterprise ideas. Objectively seize 2.0 quality vectors before stand-alone
                            </div>
                        </div>
                    </div>
                    <div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="headingfour">
                            <h4 class="panel-title">
                                <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapsefour" aria-expanded="false" aria-controls="collapsefour">
                                    Why are your rates very low compared to others?
                                </a>
                            </h4>
                        </div>
                        <div id="collapsefour" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingfour">
                            <div class="panel-body">
                                Collaboratively incubate ubiquitous e-business without enterprise ideas. Objectively seize 2.0 quality vectors before stand-alone
                            </div>
                        </div>
                    </div>
                    <div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="headingfive">
                            <h4 class="panel-title">
                                <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapsefive" aria-expanded="false" aria-controls="collapsefive">
                                    I have another question which is not listed here
                                </a>
                            </h4>
                        </div>
                        <div id="collapsefive" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingfive">
                            <div class="panel-body">
                                Collaboratively incubate ubiquitous e-business without enterprise ideas. Objectively seize 2.0 quality vectors before stand-alone
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!--================End Gallery Area =================-->

<!--================Client Slider Area =================-->
<section class="client_logo_area">
    <div class="container">
        <div class="client_slider owl-carousel">
            <div class="item">
                <img src="img/client-logo/client-1.png" alt="">
            </div>
            <div class="item">
                <img src="img/client-logo/client-2.png" alt="">
            </div>
            <div class="item">
                <img src="img/client-logo/client-3.png" alt="">
            </div>
            <div class="item">
                <img src="img/client-logo/client-4.png" alt="">
            </div>
            <div class="item">
                <img src="img/client-logo/client-5.png" alt="">
            </div>
        </div>
    </div>
</section>
<!--================End Client Slider Area =================-->




