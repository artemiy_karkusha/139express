<div class="login">
    <h1>Login</h1>
    <form name="loginform" action="/admin" method="POST">
        <input type="text" name="login" placeholder="Username" required="required" />
        <input type="password" name="password" placeholder="Password" required="required" />
        <button type="submit" class="btn btn-primary btn-block btn-large" name="submit">Log in</button>
    </form>
</div>

