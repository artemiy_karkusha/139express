-- phpMyAdmin SQL Dump
-- version 4.7.7
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Aug 05, 2018 at 01:48 PM
-- Server version: 5.7.20
-- PHP Version: 7.2.0

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `139express`
--

-- --------------------------------------------------------

--
-- Table structure for table `dispatch`
--

CREATE TABLE `dispatch` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `inv_num_china` bigint(20) DEFAULT NULL,
  `phone` varchar(100) DEFAULT NULL,
  `shipping_cost` float DEFAULT NULL,
  `scan` tinyint(1) DEFAULT NULL,
  `id_group` bigint(20) UNSIGNED DEFAULT NULL,
  `id_user` int(10) UNSIGNED DEFAULT NULL,
  `secession` varchar(100) DEFAULT NULL,
  `city` varchar(100) DEFAULT NULL,
  `first_name` varchar(100) DEFAULT NULL,
  `surname` varchar(100) DEFAULT NULL,
  `weight` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `group_dispatch`
--

CREATE TABLE `group_dispatch` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `inv_num_np` bigint(20) UNSIGNED DEFAULT NULL,
  `recipient_firstname` varchar(100) DEFAULT NULL,
  `recipient_lastname` varchar(100) DEFAULT NULL,
  `phone` varchar(100) DEFAULT NULL,
  `city` varchar(100) DEFAULT NULL,
  `secession` varchar(100) DEFAULT NULL,
  `lenght` int(10) UNSIGNED DEFAULT NULL,
  `width` int(10) UNSIGNED DEFAULT NULL,
  `heigth` int(10) UNSIGNED DEFAULT NULL,
  `actual_weigth` float UNSIGNED DEFAULT NULL,
  `volume_weigth` float UNSIGNED DEFAULT NULL,
  `shipping_cost` float DEFAULT NULL,
  `delivery_time` varchar(100) DEFAULT NULL,
  `scan` tinyint(1) NOT NULL DEFAULT '0',
  `count_delivery` int(11) NOT NULL DEFAULT '1',
  `link_print` varchar(255) DEFAULT NULL,
  `ref` varchar(255) DEFAULT NULL,
  `price` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` int(10) UNSIGNED NOT NULL,
  `firstname` varchar(100) DEFAULT NULL,
  `lastname` varchar(100) DEFAULT NULL,
  `login` varchar(64) DEFAULT NULL,
  `password` varchar(64) DEFAULT NULL,
  `law` varchar(32) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `firstname`, `lastname`, `login`, `password`, `law`) VALUES
(1, 'Artemiy', 'Karkusha', 'maestro', 'aae070341d', 'level_3'),
(2, 'Виталий', 'Коваленко', 'vitaliy001', 'qwerty001', 'level_3'),
(3, 'test', 'test', 'test', 'test', 'level_2');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `dispatch`
--
ALTER TABLE `dispatch`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_dispatch_id_group` (`id_group`),
  ADD KEY `idx_dispatch_id_user` (`id_user`);

--
-- Indexes for table `group_dispatch`
--
ALTER TABLE `group_dispatch`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `dispatch`
--
ALTER TABLE `dispatch`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=788;

--
-- AUTO_INCREMENT for table `group_dispatch`
--
ALTER TABLE `group_dispatch`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=780;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `dispatch`
--
ALTER TABLE `dispatch`
  ADD CONSTRAINT `fk_dispatch_group_dispach` FOREIGN KEY (`id_group`) REFERENCES `group_dispatch` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_dispatch_user` FOREIGN KEY (`id_user`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
